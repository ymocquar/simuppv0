//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
// Contact : yves.mocquard@irisa.fr
//

SimuPPv1b is a software to simulate population protocols.

It is a Maven Eclipse project.

The best way to get it is to import it from eclipse and make a Maven update.

The executables are in the package main.

Description of the files (each file corresponds to an object) :

Package network This package is the core of the software.

- Network.java : it is the network object, it is the object that executes a protocol
- NetworkFactory.java : it  is the factory of the network, it is the structure to initialize to define the parameters of the network.
- Node.java : it is a class used internally by NetWork
- Scheduler.java : it is the interface that defines the random generator.
- UniformSched.java : it is the simplest implementation of Scheduler
- IValue : it  is the interface that allows to define a protocol its only method is process that describes the interaction
- Link.java : it is an internal class in Network


Package impl This package defines the protocols
- HowMany.java : it is a protocol based on the average used for the tests
- ProportionWithCount.java : it is a proportion protocol with instant convergence detection, the data common to the protocol are in an implementation of the Scheduler interface.
there is also in this file an internal class called to launch a protocol.

Package log
- Log.java file can generate logs

Package main : in this package there are different main files, each one allows to generate the data for a graph.
	All this file can be launched without parameter.
	If there is a parameter, it is the seed of randoms, exactly the same interactions occur with the same seed.
	the data are generated in the log file.
	
	


Test package:
allows to test the network package.

gnuplot directory :
all files to generate graph are in this directory
	