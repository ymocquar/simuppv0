# set terminal pdf enhanced font "times,14"
set terminal epslatex
# set grid
#set format x "2^{%.0f}"
set logscale x
set ylabel "Temps parallèle de convergence"
set xlabel "Nombre d'agents $n$"
set key left top Left reverse
# set output "proportionDetc.pdf"
set output "proportionDetc.tex"
fct_1(x)=2.64*log(x)-1.78*log(0.000001)-2.*log(0.1)+1.01
fct_2(x)=2.64*log(x)-1.78*log(0.000001)-2.*log(0.001)+1.01
fct_3(x)=2.64*log(x)-1.78*log(0.000001)-2.*log(0.00001)+1.01
 plot "proportionDetc.dat" using 1:2:3:4 t "$\\eps=10^{-1}$" with yerrorlines, "" using 1:5:6:7 t "$\\eps=10^{-3}$" with yerrorlines, "" using 1:8:9:10 t "$\\eps=10^{-5}$" with yerrorlines 
# plot "proportionDetc.dat" using 1:2:3:4 t "{/Symbol e}=10^{-1}" with yerrorlines, "" using 1:5:6:7 t '{/Symbol e}=10^{-3}' with yerrorlines, "" using 1:8:9:10 t '{/Symbol e}=10^{-5}' with yerrorlines,fct_1(x) t "$\\tau'(n,\delta=10^{-6},\eps=0.1)$" with lines,fct_2(x) t "$\\tau'(n,\delta=10^{-6},\eps=0.001)$" with lines,fct_3(x) t "$\\tau'(n,\delta=10^{-6},\eps=0.00001)$" with lines
