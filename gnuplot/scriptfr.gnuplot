set terminal pdf enhanced font "times,14"
# set grid
#set format x "2^{%.0f}"
set logscale x
set ylabel "Temps parallèle de convergence"
set xlabel "Nombre d'agents n"
set key left top Left reverse
set yrange [0:40]
set output "optPropFr0.pdf"
plot "optProp0.dat" using 1:3:2:4 t "{/Symbol e}=10^{-1} (17 états)" with yerrorlines, "" using 1:6:5:7 t '{/Symbol e}=10^{-3} (1 501 états)' with yerrorlines, "" using 1:9:8:10 t '{/Symbol e}=10^{-5} (150 001 états)' with yerrorlines
# , "" using 1:11:12:13 t '{/Symbol e}=10^{-7} (15,000,001 states)' with yerrorlines
set output "optPropFr1.pdf"
plot "optProp1.dat" using 1:3:2:4 t "{/Symbol e}=10^{-1} (17 états)" with yerrorlines, "" using 1:6:5:7 t '{/Symbol e}=10^{-3} (1 501 états)' with yerrorlines, "" using 1:9:8:10 t '{/Symbol e}=10^{-5} (150 001 états)' with yerrorlines # , 0.4297*x +4, 0.4297*x +8, 0.4297*x +13.21,0.4297*x+17.21, 0.4297*x +22.42,0.4297*x+26.42
# , "" using 1:11:12:13 t '{/Symbol e}=10^{-7} (15,000,001 states)' with yerrorlines
