set terminal epslatex
# set terminal pdf enhanced font "times,12"
#set ylabel "Probability"
set xlabel "$\\delta$"
#set ylabel "Spreading parallel time from two knowings"
set ylabel "Temps parallèle de diffusion"
set key left top Left reverse
set logscale x
# set output "resultSpreadDelta.pdf"
set output "resultSpreadDelta.tex"
fct_1(x)=log(10000)-log(x)/2
fct_2(x)=log(1000)-log(x)/2
fct_3(x)=log(100)-log(x)/2
plot [0.5:0.00001] fct_1(x) t "\\scriptsize{$\\tau_1$ avec $n=10^{4}$}" with line lt 1, "spreadDelta.dat" using 1:4 title "\\scriptsize{$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $n=10^{4}$}" with linespoints lt 1, fct_2(x) t "\\scriptsize{$\\tau_1$ avec $n=10^{3}$}" with line lt 2, "spreadDelta.dat" using 1:3 title "\\scriptsize{$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $n=10^{3}$}" with linespoints lt 2,   fct_3(x) t "\\scriptsize{$\\tau_1$ avec $n=10^{2}$}" with line lt 3, "spreadDelta.dat" using 1:2 title "\\scriptsize{$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $n=10^{2}$}" with linespoints lt 3
