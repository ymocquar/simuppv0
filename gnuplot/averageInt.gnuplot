set terminal epslatex
# set terminal pdf enhanced font "times,12"
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "Nombre $n$ d'agents"
set ylabel "Temps parallèle de convergence"
set key left top Left reverse
set logscale x
# set output "resultAverageInt.pdf"
set output "resultAverageInt.tex"
fct_1(x)=1.00*log(x)-0.50*log(0.001)-1.80+2*log(100)
fct_2(x)=1.00*log(x)-0.50*log(0.01)-1.80+2*log(100)
fct_3(x)=1.00*log(x)-0.50*log(0.1)-1.80+2*log(100)
#fct_4(x)=0.62*log(x)-0.62*log(0.0001)-1.03+2*log(100)
# fct_1(x)=0.91*log(x)-0.55*log(0.1)-1.90+2*log(100)
# fct_2(x)=0.91*log(x)-0.55*log(0.01)-1.90+2*log(100)
# fct_3(x)=0.91*log(x)-0.55*log(0.001)-1.90+2*log(100)
# fct_4(x)=0.91*log(x)-0.55*log(0.0001)-1.90+2*log(100)
# fct_1(x)=0.71*log(x)-0.75*log(0.5)-1.90+2*log(100)
# fct_2(x)=0.71*log(x)-0.75*log(0.1)-1.90+2*log(100)
# fct_3(x)=0.71*log(x)-0.75*log(0.01)-1.90+2*log(100)
plot [10:1000000] fct_1(x) t "$\\tau'_2(n,\\delta=0.001,\\eps=0.01)$" with line lt 1,"averageInt.dat" using 1:4 title "$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-3}$" with linespoints lt 1, fct_2(x) t "$\\tau'_2(n,\\delta=0.01,\\eps=0.01)$" with line lt 2, "" using 1:3 title "$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-2}$" with linespoints lt 2 , fct_3(x) t "$\\tau'_2(n,\\delta=0.1,\\eps=0.01)$" with line lt 3, "" using 1:2 title "$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-1}$" with linespoints lt 3
#plot [10:200000] "averageInt.dat" using 1:2 title "$\\delta=10^{-1}$" with linespoints lt 1, "" using 1:3 title "$\\delta=10^{-2}$" with linespoints lt 2 , "" using 1:4 title "$\\delta=10^{-3}$" with linespoints lt 3, "" using 1:5 title "$\\delta=10^{-4}$" with linespoints lt 4,fct_1(x) t "$\\tau_2(n,\\delta=0.5,\\eps=0.01)$" with line lt 1, fct_2(x) t "$\\tau_2(n,\\delta=0.1,\\eps=0.01)$" with line lt 2, fct_3(x) t "$\\tau_2(n,\\delta=0.001,\\eps=0.01)$" with line lt 3, fct_4(x) t "$\\tau_2(n,\\delta=0.0001,\\eps=0.01)$" with line lt 4

