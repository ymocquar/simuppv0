# set terminal pdf enhanced font "times,12"
set terminal epslatex
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "c"
set key right top Right reverse
set yrange [0:1]
# set output "resultComp100Cont.pdf"
set output "resultComp100Cont.tex"
exactCont_100(k)=((-90463.83335227887)+k*(19208.0))*exp(-(2.0000000000000018)*k)+((-3.597725340900307*10**8)+k*(8.9413632*10**7))*exp(-(3.959595959595963)*k)+((-4.9702392657716705*10**11)+k*(1.35819940872*10**11))*exp(-(5.878787878787883)*k)+((-3.446465469301166*10**14)+k*(1.00959038492928*10**14))*exp(-(7.757575757575763)*k)+((-1.42318920850101984*10**17)+k*(4.40574556447872*10**16))*exp(-(9.595959595959592)*k)

e(x) = exactCont_100(x*5.1256)
g(x) = (1/x)*exp(-(x-1-log(x))*2.*5.1256)
f(x) = (100/98.0+2.0*x*5.1256*98.0*98.0)*exp(-(x*2.0*5.1256))
hn=5.1256
fp(c,n) = (n/(n-2) + 2. * hn * c * (n - 1) * (n - 2) * (n - 2) / n) * exp( -2. * c * hn * (n - 1)/n);
gp(c,n) = exp( -2*(c - 1. - log(c)) * hn * (n - 1)/n) / c;
#e(x) = x -1
#g(x) = x-1
#f(x) = x-1
cetoile=1.16302

set arrow from cetoile, 0 to cetoile, f(cetoile) nohead
set xtics add ("c*" cetoile)
plot [1:2] g(x) title "$\\psi(c,100)$" with linespoints, f(x) title "$\\phi(c,100)$" with linespoints, e(x) title "$\\nbP(\\Theta_{100}> c\\nbE(\\Theta_{100}))(c,100)$" 
# , fp(x,100) t 'fp' with linespoints, gp(x,100) t 'gp' with linespoints
unset xtics
unset arrow
#
#
# set output "resultComp1000Cont.pdf"
set output "resultComp1000Cont.tex"
exactCont_1000(k)=((-1.392406683382083*10**7)+k*(1992008.0))*exp(-(2.0000000000000018)*k)+((-6.18223985695753*10**12)+k*(9.89041936032*10**11))*exp(-(3.9959959959959823)*k)+((-9.5459534706740634*10**17)+k*(1.63359068195808064*10**17))*exp(-(5.987987987987942)*k)+((-7.486649788647477*10**22)+k*(1.3463782081133803*10**22))*exp(-(7.975975975975991)*k)+((-3.550693865707591*10**27)+k*(6.644564814593422*10**26))*exp(-(9.959959959959907)*k)

e(x) = exactCont_1000(7.48698*x)
g(x) = (1/x)*exp(-2.*(x-1-log(x))*7.48698)
f(x) = (2*x*(998)**2*7.48698 + 1000/998)*exp(-2*x*7.48698)
cetoile = 1.12819

set arrow from cetoile, 0 to cetoile, f(cetoile) nohead
set xtics add ("c*" cetoile)
plot [1:2] g(x) title "$\\psi(c,1000)$" with linespoints, f(x) title "$\\phi(c,1000)$" with linespoints, e(x) title  "$\\nbP(\\Theta_{1000}> c\\nbE(\\Theta_{1000}))(c,1000)$"
unset xtics
unset arrow
#
#
# set output "resultComp5000Cont.pdf"
set output "resultComp5000Cont.tex"
exactCont_5000(k)=((-4.294426517608677*10**8)+k*(4.9960008*10**7))*exp(-(1.9999999999997797)*k)+((-4.894540691544137*10**15)+k*(6.23626049680032*10**14))*exp(-(3.999199839967993)*k)+((-1.928303692234417*10**22)+k*(2.5937661335048993*10**21))*exp(-(5.997599519904084)*k)+((-3.852481138248954*10**28)+k*(5.391795672793947*10**27))*exp(-(7.995199039808054)*k)+((-4.653353530110477*10**34)+k*(6.722228802490228*10**33))*exp(-(9.991998399679902)*k)

e(x) = exactCont_5000(9.0925*x)
g(x) = (1/x)*exp(-2.*(x-1-log(x))*9.0925)
f(x) = (2.*x*(4998)**2*9.0925 + 5000./4998.)*exp(-2.*x*9.0925)
cetoile = 1.11413
set arrow from cetoile, 0 to cetoile, f(cetoile) nohead
set xtics add ("c*" cetoile)
plot [1:2] g(x) title "$\\psi(c,5000)$" with linespoints, f(x) title "$\\phi(c,5000)$" with linespoints, e(x) title  "$\\nbP(\\Theta_{5000}> c\\nbE(\\Theta_{5000}))(c,5000)$"


unset xtics
unset arrow
#
#
# set terminal pdf enhanced font "times,12"
# set yrange [1:5]
# cmp(x) = f(x)/e(x)
# set output "cmp.pdf"
# plot [1:5] cmp(x)

