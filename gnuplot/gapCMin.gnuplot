set terminal epslatex
# set terminal pdf enhanced font "times,12"
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "n"
set ylabel "Gap"
set key left top Left reverse
set logscale x
# set output "gapCMin.pdf"
set output "gapCMin.tex"
fct_1(x)=0.73*log(x)-0.73*log(0.001) + 1.5
fct_2(x)=0.73*log(x)-0.73*log(0.01) + 1.5
fct_3(x)=0.73*log(x)-0.73*log(0.1) + 1.5
plot [10:200000] fct_1(x) t "$\\tau'_3(n,\\delta=0.001)$" with line lt 1, "gapCMin.dat" using 1:4 title "$\\Gap_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-3}$" with linespoints lt 1, fct_2(x) t "$\\tau'_3(n,\\delta=0.01)$" with line lt 2, "" using 1:3 title "$\\Gap_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-2}$" with linespoints lt 2, fct_3(x) t "$\\tau'_3(n,\\delta=0.1)$" with line lt 3, "" using 1:2 title "$\\Gap_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-1}$" with linespoints lt 3

# plot [10:163840]  fct(x) title 'fct(n,10^{-4})' with linespoints, fct_N4(x) title 'fct-N4(n,10^{-4})' with linespoints, "resN4.csv" using 1:2 title "experimental" with linespoints
