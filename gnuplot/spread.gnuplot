set terminal epslatex
# set terminal pdf enhanced font "times,12"
#set ylabel "Probability"
set xlabel "Nombre $n$ d'agents"
#set ylabel "Spreading parallel time from two knowings"
set ylabel "Temps parallèle de diffusion"
set key left top Left reverse
set logscale x
# set output "resultSpread.pdf"
set output "resultSpread.tex"
fct_1(x)=log(x)+log(1000)/2
fct_2(x)=log(x)+log(100)/2
fct_3(x)=log(x)+log(10)/2
plot [10:2000000] fct_1(x) t "\\scriptsize{$\\tau_1$ avec $\\delta=10^{-3}$}" with line lt 1, "spread.dat" using 1:4 title "\\scriptsize{$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-3}$}" with linespoints lt 1, fct_2(x) t "\\scriptsize{$\\tau_1$ avec $\\delta=10^{-2}$}" with line lt 2, "" using 1:3 title "\\scriptsize{$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-2}$}" with linespoints lt 2, fct_3(x) t "\\scriptsize{$\\tau_1$ avec $\\delta=10^{-1}$}" with line lt 3, "" using 1:2 title "\\scriptsize{$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $\\delta=10^{-1}$}" with linespoints lt 3
# plot [10:163840]  fct(x) title 'fct(n,10^{-4})' with linespoints, fct_N4(x) title 'fct-N4(n,10^{-4})' with linespoints, "resN4.csv" using 1:2 title "experimental" with linespoints
