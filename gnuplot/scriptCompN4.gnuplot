set terminal epslatex
# set terminal pdf enhanced font "times,12"
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "Nombre d'agents $n$"
set ylabel "Temps parallèle de convergence"
set key left top Left reverse
set logscale x
set output "resultCompN4.tex"
fct_N4(x)=(4.0*x/(7.0*x-6.0))*(5.0*log(x)+log(208.0/3.0)-log(10**(-3)))
fct(x)=4.0*log(2)+3.0*log(x)-log(10**(-3))
plot [10:655360]  fct(x) title "$\\tau\'\'$" with linespoints, fct_N4(x) title "$\\tau\'$" with linespoints, "resN4.dat" using 1:2 title "$\\tau$ expérimental avec $n_A = n/2$" with linespoints, "" using 1:3 title "$\\tau$ expérimental avec $n_A = 3n/4$" with linespoints
# plot [10:163840]  fct(x) title 'fct(n,10^{-4})' with linespoints, fct_N4(x) title 'fct-N4(n,10^{-4})' with linespoints, "resN4.csv" using 1:2 title "experimental" with linespoints
