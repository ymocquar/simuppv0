set terminal epslatex
set output "deltaN4.tex"
#set terminal pdf enhanced font "times,12"
#
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "$\\delta$"
#set ylabel "{/Symbol t} parallel time"
set ylabel "Temps parallèle de convergence"
set key left top Left reverse
set logscale x
#set output "resultDelta.pdf"
fct_N4(x)=(4.0*1000./(7.0*1000.0-6.0))*(5.0*log(1000.0)+log(208.0/3.0)-log(x))
fct(x)=4.0*log(2)+3.0*log(1000)-log(x)
plot [0.5:0.000001] fct(x) title "$\\tau\'\'$" with linespoints, fct_N4(x) title "$\\tau\'$" with linespoints, "resDeltaN4KappaZero.dat" using 1:2 title  "$\\tau$ expérimental avec $n_A = 500$" with linespoints, "resDeltaN4KappaGrand.dat" using 1:2 title  "$\\tau$ expérimental avec $n_A = 750$" with linespoints
set output
