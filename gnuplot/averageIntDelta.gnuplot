set terminal epslatex
# set terminal pdf enhanced font "times,12"
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "$\\delta$"
set ylabel "Temps parallèle de convergence"
set key left top Left reverse
set logscale x
# set output "resultAverageIntDelta.pdf"
set output "resultAverageIntDelta.tex"
fct_1(x)=1.00*log(100000)-0.50*log(x)-1.80+2*log(100)
fct_2(x)=1.00*log(10000)-0.50*log(x)-1.80+2*log(100)
fct_3(x)=1.00*log(1000)-0.50*log(x)-1.80+2*log(100)
# fct_1(x)=0.91*log(100000)-0.55*log(x)-1.90+2*log(100)
# fct_2(x)=0.91*log(10000)-0.55*log(x)-1.90+2*log(100)
# fct_3(x)=0.91*log(1000)-0.55*log(x)-1.90+2*log(100)
plot [0.5:0.00001] fct_1(x) t "$\\tau'_2(n=10^5,\\delta,\\eps=0.01)$" linetype 1, "averageIntDelta.dat"  using 1:4 title "$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $n=10^5$" with linespoints lt 1, fct_2(x) t "$\\tau'_2(n=10^4, \\delta, \\eps=0.01)$" linetype 2, "" using 1:3 title "$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $n=10^{4}$" with linespoints linetype 2, fct_3(x) t "$\\tau'_2(n=10^3,\\delta,\\eps=0.01)$" linetype 3, "" using 1:2 title "$\\theta_{\\lceil N(1-\\delta)\\rceil}$ avec $n=10^{3}$" with linespoints linetype 3  
# plot [0.5:0.00001] "averageIntDelta.dat"  using 1:4 title "$n=10^5$" with linespoints, "" using 1:3 title "$n=10^{4}$" with linespoints, "" using 1:2 title "$n=10^{3}$" with linespoints  , fct_1(x) t "$\\tau_2(n=10^5,\\delta,\\eps=0.01)$" with line, fct_2(x) t "$\\tau_2(n=10^4, \\delta, \\eps=0.01)$" with line, fct_3(x) t "$\\tau_2(n=10^3,\\delta,\\eps=0.01)$" with line
# plot [0.5:0.00001] "averageIntDelta.dat" using 1:4 title "$n=10^{4}$" with linespoints, "" using 1:3 title "$n=10^{3}$" with linespoints, "" using 1:2 title "$n=10^{2}$" with linespoints  , fct_1(x) t "$\tau_2(n,delta)$ with $n=10^{4}$" with pi -2, fct_2(x) t "$\tau_2(n,delta) $n=10^{3}$" with pi -2, fct_3(x) t "$\tau_2(n,delta) with $n=10^{2}$" with pi -2

