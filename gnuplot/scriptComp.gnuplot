set terminal pdf enhanced font "times,12"
# set grid
# set format x "2^{%.0f}"
# set arrow from mu, 0 to mu, normal(mu, mu, sigma) nohead
# set arrow from mu, normal(mu + sigma, mu, sigma) \
#           to mu + sigma, normal(mu + sigma, mu, sigma) nohead
#           set label "mu" at mu + 0.5, ymax / 10
#           set label "sigma" at mu + 0.5 + sigma, normal(mu + sigma, mu, sigma)#
#set ylabel "Probability"
set xlabel "c"
set key right top Right reverse
set yrange [0:1]
set output "resultComp100.pdf"
exact_100(k)=((-88654.5566852333)+k*(192.08))*(0.98)**(k-1)+((-3.4552699536646587*10**8)+k*(894136.32))*(0.9604040404040404)**(k-1)+((-4.67804944226873*10**11)+k*(1.35819940872*10**9))*(0.9412121212121212)**(k-1)+((-3.1791032995614394*10**14)+k*(1.00959038492928*10**12))*(0.9224242424242424)**(k-1)+((-1.2866205470792048*10**17)+k*(4.40574556447872*10**14))*(0.9040404040404041)**(k-1)

e(x) = exact_100(512.56*x)
g(x) = (1/x)*((0.98)**((x-1-log(x))*512.56))
f(x) = (1.0+2.0*x*512.56*98.0*98.0/100)*((0.98)**((x*512.56)-1))
cetoile=1.14927
set arrow from cetoile, 0 to cetoile, f(cetoile) nohead
set xtics add ("c*" cetoile)
plot [1:2] g(x) title 'g(c,100)' with linespoints, f(x) title 'f(c,100)' with linespoints, e(x) title 'e(c,100)'
unset xtics
unset arrow
#
#
set output "resultComp1000.pdf"
exact_1000(k)=((-1.3896218700153187*10**7)+k*(1992.008))*(0.998)**(k-1)+((-6.157535651242841*10**12)+k*(9.89041936032*10**8))*(0.996004004004004)**(k-1)+((-9.4887924159577754*10**17)+k*(1.6335906819580806*10**14))*(0.9940120120120121)**(k-1)+((-7.426936449792679*10**22)+k*(1.3463782081133804*10**19))*(0.992024024024024)**(k-1)+((-3.515329096975068*10**27)+k*(6.644564814593422*10**23))*(0.9900400400400401)**(k-1)
e(x) = exact_1000(7476.98*x)
g(x) = (1/x)*((0.998)**((x-1-log(x))*7476.98))
f(x) = (1.0+2.0*x*7476.98*998.0*998.0/1000)*((0.998)**((x*7476.98)-1))
cetoile = 1.12691
set arrow from cetoile, 0 to cetoile, f(cetoile) nohead
set xtics add ("c*" cetoile)
plot [1:2] g(x) title 'g(c,1000)' with linespoints, f(x) title 'f(c,1000)' with linespoints, e(x) title 'e(c,1000)'
unset xtics
unset arrow
#
#
set output "resultComp5000.pdf"
exact_5000(k)=((-4.2927087470016336*10**8)+k*(9992.0016))*(0.9996)**(k-1)+((-4.890625842274069*10**15)+k*(1.247252099360064*10**11))*(0.9992001600320064)**(k-1)+((-1.9259906535746624*10**22)+k*(5.1875322670097984*10**17))*(0.9988004800960192)**(k-1)+((-3.846320867549473*10**28)+k*(1.0783591345587894*10**24))*(0.9984009601920384)**(k-1)+((-4.644054269905275*10**34)+k*(1.3444457604980457*10**30))*(0.998001600320064)**(k-1)

e(x) = exact_5000(45462.45*x)
g(x) = (1/x)*((0.9996)**((x-1-log(x))*45462.45))
f(x) = (1.0+2.0*x*45462.45*4998.0*4998.0/5000)*((0.9996)**((x*45462.45)-1))
cetoile = 1.11388
set arrow from cetoile, 0 to cetoile, f(cetoile) nohead
set xtics add ("c*" cetoile)
plot [1:2] g(x) title 'g(c,5000)' with linespoints, f(x) title 'f(c,5000)' with linespoints, e(x) title 'e(c,5000)'
 
 
 unset xtics
 unset arrow
