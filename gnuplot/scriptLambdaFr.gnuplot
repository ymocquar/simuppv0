set terminal epslatex
set output "optLambda.tex"
# set terminal pdf
# set output "optLambda.pdf"
# set grid
#set format x "2^{%.0f}"
set ylabel "Temps parallèle de convergence"
set xlabel "Valeur de $\\ell$"
# set key left top Left reverse
plot "optLambda.dat" using 1:3 notitle
