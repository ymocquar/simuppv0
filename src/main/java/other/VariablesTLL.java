package other;

import java.util.Arrays;

public class VariablesTLL {
	public double alpha=0.;
	public double mu=0.;
	public double rho=0.;
	public double muPrim=0.;
	public double rhoPrim=0.;
	public double gamma1=0.;
	public double gamma2=0.;
	public boolean alphaFixe=false;
	
	public double logn;
	
	public double c1;
	public double c2;
	public double c3;
	public double c4;
	public double [] t_c3=new double[5];
	public double [] t_c4=new double[6];
	
	public double getCoef1N( )
	{
		return 2./alpha;
	}
	@Override
	public String toString() {
		return "VariablesTLL [alpha=" + alpha + ", mu=" + mu + ", rho=" + rho + ", muPrim=" + muPrim + ", rhoPrim="
				+ rhoPrim + ", gamma1=" + gamma1 + ", gamma2=" + gamma2 + ", erreur=" + erreur + "]";
	}

	int erreur;

	public static VariablesTLL getVTLL()
	{
		return new VariablesTLL();
	}
	
	public String toString2() {
		return "[c1=" + c1 + ", c2=" + c2 + ", c3=" + c3 + ", c4=" + c4 + ", t_c3=" + Arrays.toString(t_c3)
				+ ", t_c4=" + Arrays.toString(t_c4) + "]";
	}
	public static VariablesTLL getVTLL(VariablesTLL from)
	{
		VariablesTLL pv=getVTLL();
		pv.alphaFixe = from.alphaFixe;
		pv.alpha = from.alpha;
		pv.mu = from.mu;
		pv.rho = from.rho;
		pv.muPrim = from.muPrim;
		pv.rhoPrim = from.rhoPrim;
		pv.gamma1 = from.gamma1;
		pv.gamma2 = from.gamma2;
		pv.erreur = from.erreur;
		pv.c3 = from.c3;
		pv.c4 = from.c4;
		pv.logn = from.logn;
		return pv;
	}
	public VariablesTLL dup()
	{
		return getVTLL(this);
	}
	boolean isGood() {
		return testAlpha() && testMu() &&testRho() &&testMuPrim() && testRhoPrim();
	}

	double getMinAlpha()
	{
		return 0.;
	}
	double getMaxAlpha()
	{ 
		return (5.-Math.sqrt(5))/10.;
	}
	
	double getMinMu()
	{
		return alpha/(1.+alpha);
	}
	double getMaxMu()
	{
		return 0.5;
	}
	double getMinRho()
	{
		return alpha/(1.-alpha);
	}
	double getMaxRho()
	{
		return (1.-2.*alpha)/(2.-3.*alpha);
	}
	double getMinMuPrim()
	{
		return mu/(1.-mu);
	}
	double getMaxMuPrim()
	{
		return 1./(1.+alpha);
	}
	double getMinRhoPrim()
	{
		return rho/(1.-rho);
	}
	double getMaxRhoPrim()
	{
		return (1.-2.*alpha)/(1.-alpha);
	}
	boolean testAlpha()
	{
		return getMinAlpha()<alpha &&alpha <getMaxAlpha();
	}
	boolean testMu(){
		return getMinMu() < mu && mu < getMaxMu();
	}
	boolean testRho(){
		return getMinRho() < rho && rho < getMaxRho();
	}
	boolean testMuPrim()
	{
		return getMinMuPrim() < muPrim && muPrim < getMaxMuPrim();	
	}
	boolean testRhoPrim(){
		return getMinRhoPrim() < rhoPrim && rhoPrim < getMaxRhoPrim();
	}
	public double getA()
	{
		return 2./alpha;
	}
	public double getB()
	{
		return (2./alpha)*Math.log(c3/(2*alpha*c4));
	}
	String strB = " $b = (4/\\alpha)\\log(c3/(2\\alpha c_4))$ & ";


	double getresult() {
		double C1 =  (1. - muPrim) * (2. + alpha )
				/ ((1.-gamma1)*(1. - muPrim * (1. + alpha) ));
		double lambda1 = (1. - mu - alpha*(2.-mu))/(2.*alpha);
		
		c1 = (1.+1./lambda1)*C1*Math.pow(C1/(mu*lambda1), mu/((1-mu)*muPrim-mu) );
		
		double C2 = (1.-rhoPrim)* (2.-2.*alpha-rhoPrim*(1.-alpha))/((1.-gamma2)*(1.-2.*alpha-rhoPrim*(1.-alpha)));
		
		double lambda2 = (1.- rho*(1.+alpha))/(2.*alpha);
		
		c2 = (1.+1./lambda2)*C2*Math.pow(C2/(rho*lambda2), rho/((1-rho)*rhoPrim-rho));
		
		
		
		t_c4[0]= mu-alpha*(1.-mu);
		t_c4[1]=rho-alpha*(1.+rho);
		t_c4[2]=  gamma1*(1.-muPrim*(alpha+1.));
		t_c4[3] = alpha*(1.-mu-alpha*(2.-mu))/(1.-mu*(1-alpha));
		t_c4[4] = gamma2*(1.-2.*alpha-rhoPrim*(1.-alpha));
		t_c4[5] = alpha*(1.-rho*(1.-alpha))/(1+2.*alpha-rho*(1.-alpha));
		
		c4 = Math.min(mu-alpha*(1.-mu), rho-alpha*(1.+rho));
		c4 = Math.min(c4, alpha*(1.-mu-alpha*(2.-mu))/(1.-mu*(1-alpha)));
		c4 = Math.min(c4, gamma1*(1.-muPrim*(alpha+1.)));
		c4 = Math.min(c4, gamma2*(1.-2.*alpha-rhoPrim*(1.-alpha)));
		c4 = Math.min( c4, alpha*(1.-rho*(1.-alpha))/(1+2.*alpha-rho*(1.-alpha)));
		
		t_c3[0] = alpha*(1.+alpha+rho*(1.+rho));
		t_c3[1] = (1.-mu)*alpha*(2.-mu);
		t_c3[2] = (alpha+c4)*alpha*c1;
		t_c3[3] = alpha*(1.+alpha);
		t_c3[4] = (alpha+c4)*alpha*c2;
		
		c3 =(alpha+c4)*alpha*Math.max(c1, c2);
		c3 = Math.max(c3, alpha*(1.+alpha+rho*(1.+rho)));
		c3 = Math.max(c3, (1.-mu)*alpha*(2.-mu));
		c3 = Math.max(c3, alpha*(1.+alpha));
		
//		return 2.*Math.log(c3/(2.*alpha*c4))/alpha;
//		return 2.*Math.log(c3/(2.*alpha*c4))/alpha + 4.*Math.log(n)/alpha;
//		return 2.*Math.log(c3/(2.*alpha*c4))/alpha + 4.*logn/alpha;
		return getB();
	}
}
