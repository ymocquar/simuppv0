package other;

import java.util.Random;

import log.Log;

public class AlgoTimerLL_MC {

	// VariablesTLL input;

	static class ExceptionBadAlea extends Exception {
		public ExceptionBadAlea() {
			// System.err.println("valeurs incorrectes");
		}
	}

	VariablesTLL output = null;

	Random random = new Random();

	double resGlob = Double.MAX_VALUE;

	double aleaIntervalle(double a, double b) throws ExceptionBadAlea {
		if ( a < b ) return a + (b - a) * random.nextDouble();
		if (a > b)
			throw new ExceptionBadAlea();
		else return a;
		
	}

	public VariablesTLL recOptimum(VariablesTLL center, double dist, int nb) {

		VariablesTLL var = VariablesTLL.getVTLL(center);
		int nbTot = (int) Math.pow(nb, 6);


		double alphaMin = center.alpha;
		double alphaMax = center.alpha;
		if (!center.alphaFixe) {
			alphaMin = Math.max(center.alpha - dist, center.getMinAlpha());
			alphaMax = Math.min(center.alpha + dist, center.getMaxAlpha());
		}
		for (int i = 0; i < nbTot; i++) {
			try {
				var.alpha = aleaIntervalle(alphaMin, alphaMax);
				double muMin = Math.max(center.mu - dist, var.getMinMu());
				double muMax = Math.min(center.mu + dist, var.getMaxMu());
				var.mu = aleaIntervalle(muMin, muMax);
				double rhoMin = Math.max(center.rho - dist, var.getMinRho());
				double rhoMax = Math.min(center.rho + dist, var.getMaxRho());
				var.rho = aleaIntervalle(rhoMin, rhoMax);
				double muPrimMin = Math.max(center.muPrim - dist, var.getMinMuPrim());
				double muPrimMax = Math.min(center.muPrim + dist, var.getMaxMuPrim());
				var.muPrim = aleaIntervalle(muPrimMin, muPrimMax);
				double rhoPrimMin = Math.max(center.rhoPrim - dist, var.getMinRhoPrim());
				double rhoPrimMax = Math.min(center.rhoPrim + dist, var.getMaxRhoPrim());
				var.rhoPrim = aleaIntervalle(rhoPrimMin, rhoPrimMax);
				double debG1 = Math.max(0., center.gamma1 - dist);
				double maxG1 = Math.min(1., center.gamma1 + dist);
				var.gamma1 = aleaIntervalle(debG1, maxG1);
				double debG2 = Math.max(0., center.gamma2 - dist);
				double maxG2 = Math.min(1., center.gamma2 + dist);
				var.gamma2 = aleaIntervalle(debG2, maxG2);
				double result = var.getresult();
				// Log.getInstance().trace("result="+result);

				if (result < resGlob) {
					var.isGood();
					if (var.erreur != 0)
						continue;
					output = VariablesTLL.getVTLL(var);
					resGlob = result;
					Log.getInstance().info("dist = "+dist+" B=" + output.getB() + " var=" + output);
					Log.getInstance().info(var.toString2());
				}

			} catch (ExceptionBadAlea e) {
			}

		}
		if (output == null)
			return center;
		return output;
	}

}
