//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;


import log.Log;

public class Network {

	private Scheduler random;
	private List<Node> nodes = new ArrayList<>();
	private List<Link<Node>> links = new ArrayList<>();
	private List<IValue> values;
	private boolean fullConnected = false;

	public boolean isFullConnected() {
		return fullConnected;
	}

	private boolean ringGraph = false;
	private int ringWidth = 1;
	private boolean connected;
	private int[][] trace;
	private boolean isTrace = false;
	private int t = 0;

	private final int nbNodes;
	private final int nbNodesM1;
	
	

	Network(NetworkFactory fact) {
		random = fact.getRandom();
		nbNodes = fact.getNbNodes();
		nbNodesM1 = nbNodes - 1;
		fullConnected = fact.isFullConnected();
		values = fact.getValues();
		if (!fullConnected) {
			for (int i = 0; i < nbNodes; i++) {
				nodes.add(new Node(i, fact.getValues().get(i), fullConnected));
			}
		}
		ringGraph = fact.isRingGraph();
		ringWidth = fact.getRingWidth();
		if (fact.isModeTrace()) {
			isTrace = true;
			trace = new int[nbNodes * 1000][2];
		}

		if (!fullConnected && !ringGraph) {
			int nbNeighborMin = fact.getNbNeighborMin();
			int nbNeighborMax = fact.getNbNeighborMax();
			int diff = nbNeighborMax - nbNeighborMin;

			int nbTent = 0;
			do {
				nodes.forEach((n) -> n.getNeighbors().clear());
				for (Node node : nodes) {
					int nbNeighbor = nbNeighborMin + ((diff > 0) ? random.nextInt(diff + 1) : 0);

					List<Node> neighbors = node.getNeighbors();

					int borne = nbNeighbor - neighbors.size();

					// if (node.getId() % 1000 == 0) {
					// System.out.println("juste avant boucle node.id=" +
					// node.getId());
					// }
					for (int i = 0; i < borne; i++) {
						Node to = getNewNeighbor(node);
						if (to != null) {
							neighbors.add(to);
							to.getNeighbors().add(node);
						} else {
							Log.getInstance().fatal("Erreur borne=" + borne + "nbNeighbor=" + nbNeighbor + "size()="
									+ node.getNeighbors().size());
						}
					}
				}
				connected = isConnex();
				// Log.getInstance().trace("NetWork constructor trace1 connex="
				// + connected);
				if (!connected && nbTent++ > 3) {
					break;
				}
			} while (!connected);
			// maintenant on garde le meme graphe et on ajoute une liaison à
			// chaque
			// fois
			while (!connected) {
				Node nodeCnx = nodes.stream().filter((n) -> n.isOk()).findAny().get();
				Node nodeNonCnx = nodes.stream().filter((n) -> !n.isOk()).findAny().get();
				nodeCnx.getNeighbors().add(nodeNonCnx);
				nodeNonCnx.getNeighbors().add(nodeCnx);
				connected = isConnex();
				Log.getInstance().trace("NetWork constructor trace2 connex=" + connected);
			}
			initLinks();
		} else {

		}
	}

	private void initLinks() {
		links.clear();
		for (Node n1 : nodes) {
			for (Node n2 : n1.getNeighbors()) {
				links.add(new Link<Node>(n1, n2));
			}
		}

	}

	private boolean isConnex() {

		nodes.forEach((n) -> n.setOk(false));

		Deque<Node> liste = new LinkedList<>();

		liste.add(nodes.get(0));
		nodes.get(0).setOk(true);
		int nb = 1;
		while (true) {
			Node node = liste.pollFirst();
			if (node == null)
				break;
			for (Node neighbor : node.getNeighbors()) {
				if (!neighbor.isOk()) {
					neighbor.setOk(true);
					nb++;
					liste.addLast(neighbor);
				}
			}
		}
		// System.out.println("Fin de isConnex nb=" + nb + " size()=" +
		// nodes.size());
		return nb == nodes.size();

	}

	private Node getNewNeighbor(Node from) {
		Node ret;
		List<Node> neighbors = from.getNeighbors();
		for (int i = 0; i < 3; i++) {
			ret = nodes.get(random.nextInt(nodes.size()));
			if (!neighbors.contains(ret) && ret != from) {
				return ret;
			}
		}

		int ind;
		int nb = nodes.size() - (from.getNeighbors().size() + 1);

		if (nb > 0) {
			ind = random.nextInt(nb);
		} else {
			return null;
		}
		int i = 0;

		for (Node node : nodes) {
			if (!neighbors.contains(node) && node != from) {
				if (i == ind) {
					return node;
				}
				i++;
			}
		}
		return null;
	}

	public int BFS(int numNode) {
		nodes.forEach(n -> n.setFmrValue(-1));

		Deque<Node> deque = new LinkedList<>();

		Node first = nodes.get(numNode);
		first.setFmrValue(0);

		deque.addLast(first);

		int nivRet = 0;

		while (true) {
			Node n = deque.pollFirst();
			if (n == null)
				break;
			int niv = n.getFmrValue() + 1;

			for (Node n2 : n.getNeighbors()) {
				if (n2.getFmrValue() == -1) {
					n2.setFmrValue(niv);
					deque.addLast(n2);
				}
			}

			nivRet = niv - 1;

		}

		return nivRet;

	}

	public int BFS2(int num) {
		int oldRes = 0;

		int valNum = num;
		while (true) {
			final int res = BFS(valNum);
			if (res == oldRes)
				break;
			Node nRes = nodes.parallelStream().filter(n -> n.getFmrValue() == res).findAny().get();

			valNum = nRes.getId();

			oldRes = res;
		}

		return oldRes;
	}

	public int BFSAll() {
		int max = 0;
		int res = 0;
		for (int i = 0; i < nodes.size(); i++) {
			res = BFS(i);
			if (res > max)
				max = res;
		}
		return max;
	}

	public int getDiametre() {
		int max = 0;
		for (int i = 0; i < 10; i++) {
			max = Math.max(max, BFS2(i));
		}
		return max;
	}

	private Node getRandomNeighbor(Node first) {
		List<Node> neighbors = first.getNeighbors();
		return neighbors.get(random.nextInt(neighbors.size()));
	}

	List<Node> getNodes() {
		return Collections.unmodifiableList(nodes);
	}

	public void fullProcess() {
		int iFirst=random.nextInt(nbNodes);
		int iSecond=random.nextInt(nbNodesM1);
		if (iSecond >= iFirst )
			iSecond++;
		IValue first = values.get(iFirst);
		IValue second = values.get(iSecond);
		first.process(second);
		// if (isTrace && t < trace.length) {
		// trace[t][0] = first.getId();
		// trace[t][1] = second.getId();
		// }
		//
		// if (isTrace && t < trace.length) {
		// trace[t][0] = first.getId();
		// trace[t][1] = second.getId();
		// }
		// t++;
	}
	public void optFullProcess() {
		int iFirst=random.nextInt(nbNodes);
		int iSecond=random.nextInt(nbNodesM1);
		if (iSecond >= iFirst )
			iSecond++;
		IValue first = values.get(iFirst);
		IValue second = values.get(iSecond);
		first.process(second);
//		if (gereEvt) {
//			evt = ((IValueEvt) first).isEvt();
//		}
		// if (isTrace && t < trace.length) {
		// trace[t][0] = first.getId();
		// trace[t][1] = second.getId();
		// }
		//
		// if (isTrace && t < trace.length) {
		// trace[t][0] = first.getId();
		// trace[t][1] = second.getId();
		// }
		// t++;
	}

	public void ringProcess() {
		int indFirst = random.nextInt(nodes.size());
		int dec = random.nextBoolean() ? 1 : -1;
		dec *= random.nextInt(ringWidth);
		int indSecond = (indFirst + nodes.size() + dec) % nodes.size();

		Node first = nodes.get(indFirst);
		Node second = nodes.get(indSecond);

		first.getValue().process(second.getValue());
		if (isTrace && t < trace.length) {
			trace[t][0] = first.getId();
			trace[t][1] = second.getId();
		}
		t++;

	}

	private boolean evt = false;

	public boolean isEvt() {
		return evt;
	}

	public void process1() {
		if (!fullConnected && !ringGraph) {
			// on tire au hasard un process a faire :
			// on prend un noeud au hasard, puis un de ses voisins au hasard
			Node first = nodes.get(random.nextInt(nodes.size()));
			Node second = getRandomNeighbor(first);

			first.getValue().process(second.getValue());
			t++;
		} else {
			if (fullConnected) {
				fullProcess();
			} else {
				ringProcess();
			}
		}
	}

	public void process2() {
		if (!fullConnected && !ringGraph) {
			// On tire au hasard un process a faire :
			// On tire au hasard un lien
			Link<Node> link = links.get(random.nextInt(links.size()));
			link.getFrom().getValue().process(link.getTo().getValue());
			if (isTrace && t < trace.length) {
				trace[t][0] = link.getFrom().getId();
				trace[t][1] = link.getTo().getId();
			}
			t++;
		} else {
			if (fullConnected) {
				fullProcess();
			} else {
				ringProcess();
			}
		}
	}

	void fullProcess(int nb) {
		for (int i = 0; i < nb; i++) {
			fullProcess();
		}
	}

	public void process1(int nb) {
		if (fullConnected) {
			fullProcess(nb);
		} else {
			for (int i = 0; i < nb; i++) {
				process1();
			}
		}
	}

	private int [] getAleaPermutation( int n )
	{
		int [] tab= new int[n];
		
		int nM1 = n-1;
		
		for ( int i = 0 ; i < n ; i++ )
		{
			tab[i] = i;
		}
		
		int j;
		int v;
		for ( int i = 0 ; i < nM1 ; i++ )
		{
			j = i+random.nextInt(n-i);
			v = tab[i];
			tab[i] = tab[j];
			tab[j] = v;	
		}
		
// Knuth shuffle		
//	    unsigned i;
//	    for (i = 0; i <= n-2; i++) {
//	        unsigned j = uniform(n-i); /* A random integer such that 0 ≤ j < n-i*/
//	        swap(permutation[i], permutation[i+j]);   /* Swap an existing element [i+j] to position [i] */
//	    }
		
		return tab;
		
	}
	
	public void processRegular(int nb) {
		for (int j = 0; j < 2; j++) {
			int [] tab = getAleaPermutation(values.size());
			int ind=0;
			for (int i = 0; i < nb / 2; i++) {
				IValue first = values.get(tab[ind++]);
				IValue second = values.get(tab[ind++]);
				first.process(second);
			}
		}
	}

	public void process2(int nb) {
		if (fullConnected) {
			fullProcess(nb);
		} else {
			for (int i = 0; i < nb; i++) {
				process2();
			}
		}

	}

}
