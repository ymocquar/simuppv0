//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import java.util.List;

/**
 * @author ymocquar network factory
 */
public class NetworkFactory {

	private int nbNodes;
	private int nbNeighborMin;

	private int nbNeighborMax;
	private Scheduler random=null;
	private List<IValue> values;
	private boolean fullConnected;
	private boolean ringGraph;
	private int ringWidth=1;
	private boolean modeTrace=false;


	/**
	 * private contructor
	 * 
	 * @param nbNodes
	 * 
	 */
	private NetworkFactory(int nbNodes) {
		this.nbNodes = nbNodes;

		nbNeighborMin = 1;
		nbNeighborMax = (int) (Math.log1p((double) nbNodes) / Math.log(2.)) + 1;

		fullConnected = false;
	}

	/**
	 * return a new instance of NetworkFactory
	 * 
	 * @param nbNodes
	 *            the number of nodes the min and max neighbors are
	 *            respectively set to 1 and (ln(nbNodes+1)/ln(2))+1 rounded to
	 *            the floor integer value.
	 * @return a new NetworkFactory object
	 */
	public static NetworkFactory getInstance(int nbNodes) {
		return new NetworkFactory(nbNodes);
	}

	private void verifyParameterConsistency() throws IllegalArgumentException, NullPointerException {
		if (!fullConnected) {
			if (nbNeighborMin < 0) {
				throw new IllegalArgumentException("nbNeighborMin must be positive or equal to zero");
			}
			if (nbNeighborMax > nbNodes) {
				throw new IllegalArgumentException("nbNeighborMax must be less or equal than number of nodes");
			}
		}
		if ( fullConnected && ringGraph )
		{
			throw new IllegalArgumentException("FullConnected is incompatible with ringGraph");		
		}

		if (values == null) {
			throw new NullPointerException("values must be initialized");
		}

		if (values.size() != nbNodes) {
			throw new IllegalArgumentException("values size must be equal to the number of nodes");
		}

	}

	/**
	 * return an instance of Network with this parameters
	 * 
	 * @return the Network object
	 * @IllegalArgumentException if nbNeightbourMin is negative
	 * @IllegalArgumentException if nbNeightbourMax is strictly bigger than
	 *                           number of nodes
	 * @NullPointerException if values is null
	 * @IllegalArgumentException if values size is different than number of
	 *                           nodes
	 */
	public Network getInstanceOfNetWork() throws IllegalArgumentException, NullPointerException {

		verifyParameterConsistency();

		return new Network(this);
	}

	/**
	 * get the number of nodes
	 * 
	 * @return the number of nodes
	 */
	public int getNbNodes() {
		return nbNodes;
	}

	/**
	 * set the number of nodes
	 * 
	 * @param nbNodes
	 *            the number of nodes
	 */
	public void setNbNodes(int nbNodes) {
		this.nbNodes = nbNodes;
	}

	/**
	 * get the number min of neighbors
	 * 
	 * @return the number min of neighbors
	 */
	public int getNbNeighborMin() {
		return nbNeighborMin;
	}

	/**
	 * set the number min of neighbors
	 * 
	 * @param nbNeighborMin
	 *            the number min of neighbors
	 */
	public void setNbNeighborMin(int nbNeighborMin) {
		if (nbNeighborMin > nbNeighborMax) {
			nbNeighborMax = nbNeighborMin;
		}
		this.nbNeighborMin = nbNeighborMin;
	}

	/**
	 * get the number max of neighbors
	 * 
	 * @return the number max of neighbors
	 */
	public int getNbNeighborMax() {
		return nbNeighborMax;
	}

	/**
	 * set the number max of neighbors
	 * 
	 * @param nbNeighborMax
	 *            the number max of neighbors
	 */
	public void setNbNeighborMax(int nbNeighborMax) {
		if (nbNeighborMax < nbNeighborMin) {
			nbNeighborMin = nbNeighborMax;
		}
		this.nbNeighborMax = nbNeighborMax;
	}

	/**
	 * get the list of values
	 * 
	 * @return the list of values
	 */
	public List<IValue> getValues() {
		return values;
	}

	/**
	 * set the list of values
	 * 
	 * @param values
	 *            list of values
	 * @exception IllegalArgumentException
	 *                if the size of the list is different than the number of
	 *                nodes.
	 */
	public void setValues(List<IValue> values) throws IllegalArgumentException {
		if (values.size() != nbNodes) {
			throw new IllegalArgumentException("The sizeof values list is " + values.size() + " it must be " + nbNodes
					+ " like nbNodes");
		}
		this.values = values;
	}

	/**
	 * set the netWork to be ( or not ) full connected
	 * 
	 * @param fullConnected
	 */
	public void setFullConnected(boolean fullConnected) {
		this.fullConnected = fullConnected;
	}

	/**
	 * is the network will be full connected
	 * 
	 * @return
	 */
	public boolean isFullConnected() {
		return fullConnected;
	}
	/**
	 * set the netWork to be ( or not ) a ring graph
	 * 
	 * @param ringGraph
	 */
	public void setRingGraph(boolean ringGraph) {
		this.ringGraph = ringGraph;
	}

	/**
	 * is the network will be a ring Graph
	 * 
	 * @return
	 */
	public boolean isRingGraph() {
		return ringGraph;
	}

	/**
	 * get the random generator
	 * 
	 * @return
	 */
	public Scheduler getRandom() {
		if ( random == null )
		{
			random = new UniformSched();
		}

		return random;
	}
	public void setRandom( Scheduler random )
	{
		this.random = random;
	}

	public int getRingWidth() {
		return ringWidth;
	}

	public void setRingWidth(int ringWidth) {
		this.ringWidth = ringWidth;
	}

	public boolean isModeTrace() {
		return modeTrace;
	}

	public void setModeTrace(boolean modeTrace) {
		this.modeTrace = modeTrace;
	}

	public void setScheduler(Scheduler random2) {
		random = random2;
		
	}


}
