//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
//
package log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Singleton Log Tool
 */
public class Log {
	private Logger logManager = LogManager.getLogger();
	static private Log log=new Log();
	
	public static Log getInstance() {
		return log;
	}

	private <T>  String getTab( T tab[], int deb, int borne )
	{
		String str="";
		for ( int  i = deb ; i < borne ; i++ )
		{
			str += tab[i];
			str += ",";
		}
		return str;
	}

	public void trace(String msg) {
		logManager.trace(msg);
	}
	public <T> void trace(String msg, T [] tab )
	{
		trace(msg+getTab(tab,0,tab.length));
	}
	public <T> void trace(String msg, T [] tab, int borne )
	{
		trace(msg+getTab(tab,0,borne));
	}
	public <T> void trace(String msg, T [] tab, int deb, int borne )
	{
		trace(msg+getTab(tab, deb, borne));
	}

	
	public void debug(String msg) {
		logManager.debug(msg);
	}
	public void info(String msg) {
		logManager.info(msg);
	}
	public void warn(String msg)
	{
		logManager.warn(msg);
	}

	public void error(String msg) {
		logManager.error(msg);
	}
	public void fatal(String msg) {
		logManager.fatal(msg);
	}
}
