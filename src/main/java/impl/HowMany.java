//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package impl;

import network.IValue;

/**
 * @author ymocquar
 * Implementation of IValue interface
 * process perform the average and put the new val to the two values
 */
public class HowMany implements IValue {

	private double val;
	private int cpt=0;
	
	/* (non-Javadoc)
	 * @see fr.irisa.cidre.popprot.network.IValue#process(fr.irisa.cidre.popprot.network.IValue)
	 */
	@Override
	public void process(IValue value) {
		
		
		HowMany v = (HowMany)value;
		
		val = (v.val + val)/2.; // perform the average and set the new val to this and value
		v.val = val;
		cpt++;
		v.cpt++;
	}

	/**
	 * constructor 
	 */
	public HowMany() {
		this.val = 0.;
	}
	


	public int getCpt() {
		return cpt;
	}

	public void setCpt(int cpt) {
		this.cpt = cpt;
	}

	/**
	 * get the val
	 * @return
	 * 	 val
	 */
	public double getVal() {
		return val;
	}

	/**
	 * set the val
	 * @param val
	 * 
	 */
	public void setVal(double val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return "HowMany [val=" + val + "]";
	}

}
