package impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import network.IValue;
import network.Scheduler;

public class CountMinWithGap implements IValue {
	
	static public class GapCountMin implements Scheduler {
		private Random random;
		public final int n;
		public int m=0;
		public int [] nb;
		public int max=0;
		public List<Integer> result=new ArrayList<>();
		public GapCountMin( int n)
		{
			random=new Random();
			this.n = n;
			nb = new int[100];
		}
		public void dec(int val)
		{
			int v=val+m;
			nb[v]--;
			if ( nb[v] == 0 && v == 0 )
			{
					m--;
					int i = 0;
					do
					{
						nb[i] = nb[i+1];
						i++;
					}while( i <= max );
					max--;
			}

		}
		@Override
		public String toString() {
			return "DataCountAverageInt [nb=" + Arrays.toString(nb) + ", max=" + max + "]";
		}
		public void add( int val)
		{
			int v = val+m;
			nb[v]++;
			if ( v > max )
				max = v;
		}

		public int getGap()
		{
			return max;
		}
		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
			
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}
		@Override
		public void setSeed(long seed) {
			random.setSeed(seed);
			
		}
	}
	
	GapCountMin refDC;

	public CountMinWithGap( GapCountMin dc)
	{
		refDC = dc;
		refDC.nb[0]++;
	}
	
	public int val=0;
	@Override
	public void process(IValue value) {
		CountMinWithGap v=(CountMinWithGap)value;
		if ( val > v.val )
		{
			refDC.add(v.val+1);
			refDC.dec(v.val);
			v.val++;
		}
		else
		{
			refDC.add(val+1);
			refDC.dec(val);
			val++;
		}
	}

}
