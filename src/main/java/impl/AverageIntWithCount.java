package impl;

import java.util.Arrays;
import java.util.Random;

import log.Log;
import network.IValue;
import network.Scheduler;

public class AverageIntWithCount implements IValue {
	
	static public class DataCountAverageInt implements Scheduler {
		private Random random;
		public final int n;
		public final int m;
		public double ell;
		public int [] nb;
		public int min=0;
		public int max=0;
		public double result;
		public DataCountAverageInt( int n, int m )
		{
			random=new Random();
			this.n = n;
			this.m = m;
			nb = new int[2*m+1];
		}
		public void dec(int val)
		{
			int v=val+m;
			nb[v]--;
			if ( nb[v] == 0 )
			{
				if ( v == min )
				{
					do
					{
						v++;
					}while( nb[v] == 0);
					min = v;
				}
				else
				{
					if ( v == max )
					{
						do
						{
							v--;
						}while( nb[v] == 0);
						max = v;
					}
					
				}
			}
		}
		@Override
		public String toString() {
			return "DataCountAverageInt [nb=" + Arrays.toString(nb) + ", min=" + min + ", max=" + max + "]";
		}
		public void add( int v)
		{
			nb[v+m]++;
		}
		public void init( int v)
		{
			add(v);
			if ( v > 0 )
				max = v+m;
			else
				min = v+m;
		}
		public void verif()
		{
			int nbb=0;
			for ( int i=min ; i <= max ; i++ )
			{
				nbb += nb[i];
			}
			if ( nbb != n)
			{
				Log.getInstance().error("TRACE "+this);
			}
		}
		public long getDiff()
		{
			return max-min;
		}
		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
			
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}
		@Override
		public void setSeed(long seed) {
			// TODO Auto-generated method stub
			
		}
	}
	
	DataCountAverageInt refDC;

	public AverageIntWithCount( DataCountAverageInt dc, int sens )
	{
		refDC = dc;
		val = sens*refDC.m;
		refDC.init(val);
	}
	public AverageIntWithCount( DataCountAverageInt dc, int sens, boolean pUn )
	{
		refDC = dc;
		val = sens*refDC.m;
		if ( pUn ) val++;
		refDC.init(val);
	}
	
	public int val;
	@Override
	public void process(IValue value) {
		AverageIntWithCount v=(AverageIntWithCount)value;
		int oldVal1 = val;
		int oldVal2 = v.val;
		
		int sum = v.val+val;
		v.val = sum/2;
		val = sum - v.val;
		
		refDC.add(val);
		refDC.add(v.val);
		refDC.dec(oldVal1);
		refDC.dec(oldVal2);
	}

}
