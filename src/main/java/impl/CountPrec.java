package impl;

import java.util.Random;

import network.IValue;
import network.Scheduler;

public class CountPrec implements IValue {
	
	public final int m;
	public int val;
	
	static public class DataCount implements Scheduler {
		private Random random;
		public final int m;
		public int [] nb;
		int vmin=0;
		int vmax=0;
		int nbInter=0;
		public DataCount( int m )
		{
			random=new Random();
			nb = new int[m*2+1];
			this.m = m;
			vmin=-m;
			vmax=m;
		}
		public void add(int v)
		{
			nb[v+m]++;
		}
		public void decr( int v)
		{
			nb[v+m]--;
		}
		public void traitMinMax()
		{
			nbInter++;
			while (nb[vmin+m] == 0) {
				vmin++;
			}
			while (nb[vmax+m] == 0) {
				vmax--;
			}

		}
		public int getGap()
		{
			return vmax-vmin;
		}
		public int getNbInter()
		{
			return nbInter;
		}
		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}
		@Override
		public void setSeed(long seed) {
			random.setSeed(seed);
			
		}
		
	}
	DataCount refDC;
	
	public CountPrec( DataCount dc, int m, int sens )
	{
		this.m = m;
		val = sens*m;
		refDC=dc;
		refDC.add(val);
	}

	@Override
	public void process(IValue value) {
		CountPrec v = (CountPrec)value;
		
		refDC.decr(val);
		refDC.decr(v.val);
		
		int sum = val+v.val;
		val = sum/2;
		v.val = sum-val;
		refDC.add(val);
		refDC.add(v.val);
		refDC.traitMinMax();
	}
	
	public long getVal()
	{
		return val;
	}
	public double getVal2()
	{
		return (double)val-0.5;
	}

}
