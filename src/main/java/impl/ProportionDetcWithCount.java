package impl;

import java.util.Random;

import network.IValue;
import network.Scheduler;

public class ProportionDetcWithCount implements IValue {

	static public class  DataCountProportion implements Scheduler {
		final Random random;
		final int Tmax;
		public final int m;
		public final int n;
		
		public double result=0.;
		
		int nbS=0;
		
		public void add()
		{
			nbS++;
		}
		public int getNbS()
		{
			return nbS;
		}
		
		public DataCountProportion(  int n, double delta, double eps )
		{
			random = new Random();
			m = (int)Math.ceil(3./(4.*eps));
//			1.64\ln n -1.28\ln\delta -2\ln\eps +1.01\
//			Tmax = (int)Math.ceil(12.78*Math.log(n)-17.60*Math.log(delta)+66.57);
			Tmax = (int)Math.ceil(1.73*Math.log(n)-1.23*Math.log(delta)-2*Math.log(eps)+1.05);
			this.n = n;
		}

		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}
		@Override
		public void setSeed(long seed) {
			random.setSeed( seed);
			
		}
		
		
	}
	
	
	int t=0;
	int val=0;
	boolean s=false;
	
	DataCountProportion refDC;
	
	
	public ProportionDetcWithCount(  DataCountProportion dc, int sens )
	{
		refDC = dc;
		val = refDC.m*sens;
	}
	
	@Override
	public void process(IValue value) {
		ProportionDetcWithCount v = (ProportionDetcWithCount)value;
		if ( s || v.s || ( t == refDC.Tmax-1 && v.t == refDC.Tmax-1 ) )
		{
			if ( !s) refDC.add();
			if ( !v.s) refDC.add();
			s = true;
			v.s = true;
		}
		else
		{
			if ( t > v.t )
			{
				v.t++;
			}
			else
			{
				t++;
			}
			int sum = val+v.val;
			val = sum/2;
			v.val = sum-val;
		}

	}
	public double getResult()
	{
		double dm=(double)refDC.m;
		return (dm +((double) val))/(2.*dm);
		
	}

}
