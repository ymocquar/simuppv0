package impl;

import network.IValue;
public class CountMinSimple implements IValue {
	public int val = 0;


	@Override
	public void process(IValue value) {
		CountMinSimple v = (CountMinSimple) value;
		if (v.val > val) {
			val++;

		} else {
			v.val++;
		}
	}

	public int getResult() {
		return val;
	}
}
