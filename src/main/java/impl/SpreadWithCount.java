//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard 
// 
// AverageLongWithCount
//
// Objet de base pour le protocole basé sur la moyenne
//
// un autre objet est défini à l'interieur : il s'agi d'un objet implémentant le cheduler 
// et gardera les donne globale 

package impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;

public class SpreadWithCount implements IValue {

	static public class DataCountSpread implements Scheduler {
		private Random random;
		public final int n;
		public double result;

		private int nbGood = 0;

		public DataCountSpread(int n, long seed) {
			random = new Random();
			this.n = n;
			setSeed(seed);
		}

		public boolean isConverge() {
			return nbGood == n;
		}


		@Override
		public int nextInt(int borne) {
			return random.nextInt(borne);
		}

		@Override
		public boolean nextBoolean() {
			return random.nextBoolean();
		}

		@Override
		public void setSeed(long seed) {
			random.setSeed(seed);
		}
	}

	// Méthode appelée par le stream
	// Cette méthode lance un protocole.
	// random implemente l'interface Scheduler, c'est ala fois un genrateur
	// aleatoire et aussi
	// permet de centralisé les donnée propore à ce protocole comme le nombre
	// d'agents ayant le bon résultat.
	// Le but étant de détecter précisément l'instant de la convergence.
	//
	public static void executePP(SpreadWithCount.DataCountSpread random, int nb) {
		List<IValue> values = new ArrayList<>();
		int n = random.n;

		NetworkFactory fact = NetworkFactory.getInstance(n);

		for (int i = 0; i < n; i++) {
			values.add(new SpreadWithCount(random, i < nb));
		}

		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		long borneSec = (long) (Math.log(n) * 100 * n);
		long k = 0;
		while (!random.isConverge()) {
			netWork.process1();
			k++;
			if (k > borneSec) {
				Log.getInstance().error("ERREUR k=" + k + " borneSec =" + borneSec + " r=" + random);
				Log.getInstance().error("ABORT");
				System.exit(-1);
				break;
			}
		}

		random.result = ((double) k) / ((double) n);
	}

	DataCountSpread refDC;

	public SpreadWithCount(DataCountSpread dc, boolean is) {
		refDC = dc;
		if (is) {
			refDC.nbGood++;
		}
		this.is = is;
	}


	public boolean is;

	@Override
	public void process(IValue value) {
		SpreadWithCount v = (SpreadWithCount) value;

		int locNbGood = (is ? 1 : 0) + (v.is ? 1 : 0);

		is = v.is = (is || v.is);
		
		int plus = (is ? 1 : 0) + (v.is ? 1 : 0) - locNbGood;

		refDC.nbGood += plus;

	}

}
