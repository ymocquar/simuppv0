package impl;
import network.IValue;
public class AverageDouble implements IValue {
	final private double n;
	public double val; // etat
	public AverageDouble(int n, double val2) {
		val = val2;
		this.n = n;
	}
	@Override
	public void process(IValue value) {
		AverageDouble v = (AverageDouble) value;
		double sum = v.val + val;
		val = sum/2.;
		v.val = sum - val;
	}
	public int getInt(double bMin, double bMax, int nb)
	{
		int v = (int)Math.round(((val-bMin)*nb)/(bMax-bMin));
		return (v < 0 ) ? 0 : (v > nb ) ? nb : v;
	}
	public int getResult()
	{
		return (int)Math.floor(val*n+0.5);
	}
	
}

