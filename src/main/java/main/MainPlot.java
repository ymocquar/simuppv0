package main;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;

import log.Log;

/* ce programme permet de généré les coefficent de la loi du temps de diffusion */

public class MainPlot {

	public static class SpeedExact {
		final int n;
		final BigDecimal[] p;
		final BigDecimal[] coef;
		final int nm1;
		final double nNm1;
		final int ns2;
		final int ns2p1;
		final MathContext mc;
//		final int scale=-50;

		BigDecimal[] memRes;

		SpeedExact(int n) {
			mc = new MathContext(3 * n / 4, RoundingMode.HALF_EVEN);
			this.n = n;
			nm1 = n - 1;
			nNm1 = (double) (n * nm1);
			BigDecimal bdNnm1 = BigDecimal.valueOf(n * nm1);
			ns2 = n / 2;
			ns2p1 = ns2 + 1;

			p = new BigDecimal[n];
			coef = new BigDecimal[n];
			for (int i = 1; i < n; i++) {
				p[i] = (BigDecimal.valueOf(2 * i * (n - i))).divide(bdNnm1, mc);
				coef[i] = BigDecimal.ONE.subtract(p[i], mc);
			}
			memRes = new BigDecimal[ns2p1];
			// par default on ne memorise que 1
			indmem1 = 1;
			indmem2 = 0;
			indmem3 = 0;

		}

		public int getIndmem1() {
			return indmem1;
		}

		public void setIndmem1(int indmem1) {
			this.indmem1 = indmem1;
		}

		public int getIndmem2() {
			return indmem2;
		}

		public void setIndmem2(int indmem2) {
			this.indmem2 = indmem2;
		}

		public int getIndmem3() {
			return indmem3;
		}

		public void setIndmem3(int indmem3) {
			this.indmem3 = indmem3;
		}

		private int indmem1;
		private int indmem2;
		private int indmem3;
		private BigDecimal[] cMem1;
		private BigDecimal[] cMem2;
		private BigDecimal[] cMem3;
		private BigDecimal[] dMem1;
		private BigDecimal[] dMem2;
		private BigDecimal[] dMem3;

		public BigDecimal getMaxC() {
			return maxC;
		}

		private BigDecimal maxC = BigDecimal.ZERO;

		void initCoef() {
			BigDecimal[][] c = new BigDecimal[2][ns2p1];
			BigDecimal[][] d = new BigDecimal[2][ns2p1];
			for (int j = 0; j <= ns2; j++) {
				c[0][j] = c[1][j] = d[0][j] = d[1][j] = BigDecimal.ZERO;
			}
			c[1][1] = BigDecimal.ONE;
			for (int ind = n - 2; ind > 0; ind--) {
				int i = n - ind;
				int ii = ind % 2;
				int im1 = (ind - 1) % 2;
				BigDecimal sum = BigDecimal.ZERO;

				for (int j = 1; j <= ns2; j++) {
					if (i != j && n - i != j) {

						if (i > j) {
							if (!d[im1][j].equals(BigDecimal.ZERO)) {
								c[ii][j] = c[im1][j].multiply(p[i]).divide(p[i].subtract(p[j]), mc).subtract(
										d[im1][j].multiply(p[i], mc).divide(p[i].subtract(p[j], mc).pow(2, mc), mc),
										mc);
							} else {
								c[ii][j] = c[im1][j].multiply(p[i]).divide(p[i].subtract(p[j]), mc);
							}
//							Log.getInstance().trace("i=" + i + " j=" + j + " c=" + c[ii][j]);

							sum = sum.add(c[ii][j]);
						} else
							c[ii][j] = BigDecimal.ZERO;

						if (i > n - j) {
							d[ii][j] = d[im1][j].multiply(p[i], mc).divide(p[i].subtract(p[j]), mc);
						} else
							d[ii][j] = BigDecimal.ZERO;
						maxC = maxC.max(c[ii][j].abs());
					}
				}
				if (i <= ns2) {
					c[ii][i] = BigDecimal.ONE.subtract(sum);
//					Log.getInstance().trace("i=" + i + " j=" + i + " c=" + c[ii][i]);

				} else {
					c[ii][ind] = BigDecimal.ONE.subtract(sum);
					d[ii][ind] = c[im1][ind].multiply(p[i]);

				}
				if (ind == indmem1) {
					cMem1 = Arrays.copyOf(c[ii], c[ii].length);
					dMem1 = Arrays.copyOf(d[ii], d[ii].length);
				}

				if (ind == indmem2) {
					cMem2 = Arrays.copyOf(c[ii], c[ii].length);
					dMem2 = Arrays.copyOf(d[ii], d[ii].length);
				}
				if (ind == indmem3) {
					cMem3 = Arrays.copyOf(c[ii], c[ii].length);
					dMem3 = Arrays.copyOf(d[ii], d[ii].length);
				}
				if (n > 2000 && i % 10 == 0) {
					Log.getInstance().info("SpeedExact trace interne init i=" + i);
				}

			}
		}

		double getC(int i, int j) {
			BigDecimal[] c = null;
			boolean flOk = false;

			if (i == indmem1) {
				c = cMem1;
				flOk = true;
			}
			if (i == indmem2) {
				c = cMem2;
				flOk = true;
			}
			if (i == indmem3) {
				c = cMem3;
				flOk = true;
			}
			return (flOk) ? c[j].doubleValue() : -1.;
		}

		double get1mP(int j) {
			return coef[j].doubleValue();
		}

		double getP(int j) {
			return 1. - coef[j].doubleValue();
		}

		double getD(int i, int j) {
			BigDecimal[] d = null;
			boolean flOk = false;

			if (i == indmem1) {
				d = dMem1;
				flOk = true;
			}
			if (i == indmem2) {
				d = dMem2;
				flOk = true;
			}
			if (i == indmem3) {
				d = dMem3;
				flOk = true;
			}
			return (flOk) ? d[j].doubleValue() : -1.;
		}

		private int nbGood = 0;

		double getV(int i, int k) {
			BigDecimal[] c = null;
			BigDecimal[] d = null;
			boolean flOk = false;
			if (i == indmem1) {
				c = cMem1;
				d = dMem1;
				flOk = true;
			}
			if (i == indmem2) {
				c = cMem2;
				d = dMem2;
				flOk = true;
			}
			if (i == indmem3) {
				c = cMem3;
				d = dMem3;
				flOk = true;
			}
			if (!flOk) {
				return -1.;
			}
			BigDecimal res = BigDecimal.ZERO;
			BigDecimal bK = new BigDecimal(k);
			BigDecimal borne = BigDecimal.ONE.divide(BigDecimal.TEN.pow(10).multiply(new BigDecimal(n)), mc);
			nbGood = 0;
			for (int j = 1; j <= ns2; j++) {
				memRes[j] = c[j].multiply(coef[j]).add(d[j].multiply(bK)).multiply(coef[j].pow(k - 1, mc));
				if (memRes[j].compareTo(borne) > 0)
					nbGood++;
				else
					break;
				res = res.add(memRes[j]);
			}
			return res.doubleValue();
		}

		double getV2(int i, int k) {
			BigDecimal[] c = null;
			BigDecimal[] d = null;
			boolean flOk = false;
			if (i == indmem1) {
				c = cMem1;
				d = dMem1;
				flOk = true;
			}
			if (i == indmem2) {
				c = cMem2;
				d = dMem2;
				flOk = true;
			}
			if (i == indmem3) {
				c = cMem3;
				d = dMem3;
				flOk = true;
			}
			if (!flOk) {
				return -1.;
			}
			BigDecimal res = BigDecimal.ZERO;
			BigDecimal bK = new BigDecimal(k);
			BigDecimal borne = BigDecimal.ONE.divide(BigDecimal.TEN.pow(14).multiply(new BigDecimal(n)), mc);
			nbGood = 0;
			for (int j = 1; j <= ns2; j++) {
				memRes[j] = p[j].multiply(c[j]).multiply(coef[j])
						.add(d[j].multiply(bK.multiply(p[j]).subtract(BigDecimal.ONE)))
						.multiply(coef[j].pow(k - 2, mc));
				if (memRes[j].compareTo(borne) > 0)
					nbGood++;
				else
					break;
				res = res.add(memRes[j]);
			}
			return res.doubleValue();
		}

		double getMem(int j) {
			return memRes[j].doubleValue();
		}

		int getNbGood() {
			return nbGood;
		}

		String getStrValue(double v) {
			String str = "" + v;
			String str2 = str.replace("E", "*10^");
			return "(" + str2 + ")";
		}

		String getFormule(int nbJ, String varK) {
			String str = "";
			for (int j = 1; j <= nbJ; j++) {
				str += "(" + getStrValue(getC(1, j) * get1mP(j)) + "+" + varK + "*" + getStrValue(getD(1, j)) + ")*"
						+ getStrValue(get1mP(j)) + "^(" + varK + "-1)";
				if (j != nbJ) {
					str += "+";
				}
			}
			return str;
		}

		String getFormuleCont(int nbJ, String varK) {
			String str = "";
			for (int j = 1; j <= nbJ; j++) {
				str += "(" + getStrValue(getC(1, j)) + "+" + varK + "*" + getStrValue(getD(1, j) * n) + ")*Math.exp(-"
						+ getStrValue(getP(j) * n) + "*" + varK + ")";
				if (j != nbJ) {
					str += "+";
				}
			}
			return str;
		}

	}

	static double H(int v) {
		double res = 0;

		for (double k = 1.; k <= (double) v; k += 1.) {
			res += 1. / k;
		}
		return res;
	}

	static double getEx(double c, int n, double hn) {
		return (Math.log(2) + Math.log(hn)) / (2 * Math.log((double) n) - 1);
	}

	public static double f1(double c, int n, double hn) {
		return ((1. + 2. * hn * c * (n - 1) * (n - 2) * (n - 2) / n) * Math.pow(1. - 2. / n, (c * hn * (n - 1)) - 1.));
	}

	public static double f2(double c, int n, double hn) {
		return (Math.pow(1. - 2. / n, (c - 1. - Math.log(c)) * hn * (n - 1))) / c;
	}
	public static double getCetoile(final int n) {
		double c = 1.01;
		final double inc = 0.00001;
		final double hn = H(n - 1);
		while (c <= 2. && f1(c, n, hn) > f2(c, n, hn))
			c += inc;
		return c - inc;
	}


	public static double f1cont(double c, int n, double hn) {
		return (((double)n)/(n-2) + 2. * hn * c * (n - 1) * (n - 2) * (n - 2) / n) * Math.exp( -2. * c * hn * (n - 1)/n);
	}

	public static double f2cont(double c, int n, double hn) {
		return Math.exp( - 2.*(c - 1. - Math.log(c)) * hn * (n - 1)/n) / c;
	}

	public static double getCetoileCont(final int n) {
		double c = 1.01;
		final double inc = 0.00001;
		final double hn = H(n - 1);
		while (c <= 2. && f1cont(c, n, hn) > f2cont(c, n, hn))
			c += inc;
		return c - inc;
	}

	public static void main(String[] args) {

		// calcul des valeurs de c*
		String strN = "n";
		String strCet = "c*\t";
		for (int n = 10; n <= 1000000000; n *= 10) {
			strN += "\t" + n;
			strCet += " & " + ((float) getCetoile(n));
		}
		Log.getInstance().trace(strN);
		Log.getInstance().trace(strCet);

		strN = "n";
		strCet = "c*\t";
		for (int n = 10; n <= 1000000000; n *= 10) {
			strN += "\t" + n;
			strCet += " & " + ((float) getCetoileCont(n));
		}
		Log.getInstance().trace("Cas continu");
		Log.getInstance().trace(strN);
		Log.getInstance().trace(strCet);

		int tabN[] = { 100, 1000, 5000 };

		for (int i = 0; i < tabN.length; i++) {
			final int n = tabN[i];
			final double hn = H(n - 1);
			Log.getInstance().info("n=" + n + " c*=" + ((float) getCetoile(n)) + " (n-1)*H_(n-1)=" + hn * (n - 1)+ " (n-1)H_(n-1)/n="+hn*(n-1)/n+" c*cont="+((float) getCetoileCont(n)));
		}

		for (int i = 0; i < tabN.length; i++) {
			final int n = tabN[i];

			SpeedExact sp = new SpeedExact(n);

			sp.initCoef();

			Log.getInstance().info("n=" + n + " fct(k)=" + sp.getFormule(5, "k"));
			Log.getInstance().info("n=" + n + " fctCont(k)=" + sp.getFormuleCont(5, "k"));

		}

	}

}
