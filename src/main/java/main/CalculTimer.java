package main;

import java.util.ArrayList;
import java.util.List;

import other.AlgoTimerLL_MC;
import other.VariablesTLL;
import log.Log;

public class CalculTimer {

	public static void main(String[] args) {


		List<VariablesTLL> sol = new ArrayList<VariablesTLL>();

		double alph=0.16;
		while (alph<=0.270001) {

			VariablesTLL var = VariablesTLL.getVTLL();

			var.alpha = (alph<0.2701)?alph:0.275;
			var.mu = 0.20;
			var.rho = 0.20;
			var.muPrim = 0.5;
			var.rhoPrim = 0.5;
			var.gamma1 = 0.5;
			var.gamma2 = 0.5;
			var.alphaFixe = true;

			double dist = 0.5;

			while( dist > 0.0000000001) {
//				AlgoTimerLL algT = new AlgoTimerLL();
//
//				var = algT.recOptimum(var, dist, 8);
				
				AlgoTimerLL_MC algT = new AlgoTimerLL_MC();

				var = algT.recOptimum(var, dist,5);

				dist /= 1.5;// expres un nombre modulo de rien
				Log.getInstance().trace(" dist=" + dist);
			}
			sol.add(var.dup());
			
			String strAlpha="$\\alpha$ & ";
			String strA = " $a = 2/\\alpha$ & ";
			String strB = " $b = (2/\\alpha)\\log(c3/(2\\alpha c_4))$ & ";
			String strLogN = "log n optimal & ";
					
			for ( VariablesTLL v: sol )
			{
				strAlpha += String.format(" %2G &", v.alpha);
				strA += ""+String.format(" %2G &", v.getA());
				strB += ""+String.format(" %2G &", v.getB());
				strLogN += " "+v.logn+" & ";
			}
			Log.getInstance().trace(strAlpha);
			Log.getInstance().trace(strA);
			Log.getInstance().trace(strB);
			Log.getInstance().trace(strLogN);
			
			alph += 0.01;
		}

	}

}
