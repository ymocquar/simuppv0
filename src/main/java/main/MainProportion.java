package main;

import java.util.ArrayList;
import java.util.List;

import impl.CountPrec;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;

public class MainProportion {

	static class Result {
		int n;
		int m;
		double tMin;
		double tMax;
		double tAverage;
	}

	public static double testAverage(int mod, int n, CountPrec.DataCount random) {
		List<IValue> values = new ArrayList<>();

		NetworkFactory fact = NetworkFactory.getInstance(n);

		for (int i = 0; i < n; i++) {
			values.add(new CountPrec(random, random.m, (i % mod == 0) ? -1 : 1));
		}
		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		int k = 0;
		while (random.getGap() > 2) {
			netWork.process1();
			k++;
		}
		return ((double) k) / ((double) n);
	}

	public static void main(String[] args) {
		int nbTest = 100;
		int paquetTest = 20;
		int[] tab_m = { 8, 750, 75000 };
		int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000,
				1000000, 2000000, 5000000, 10000000 };

		Result[][] tabResult = new Result[tab_n.length][3];

		for (int mod = 2; mod < 5; mod *= 2) {

			for (int i_n = 0; i_n < tab_n.length; i_n++) {

				int n = tab_n[i_n];

				for (int i0 = 0; i0 < 3; i0++) {

					Result res = tabResult[i_n][i0] = new Result();
					int m = tab_m[i0];
					res.m = m;
					res.n = n;
					res.tMin = 10000.;
					for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

						List<CountPrec.DataCount> tab = new ArrayList<>();

						for (int i = 0; i < paquetTest; i++) {
							tab.add(new CountPrec.DataCount(m));
						}

						final int nPar = n;
						final int modPar = mod;

						tab.parallelStream().forEach(r -> testAverage(modPar, nPar, r));

						for (CountPrec.DataCount s : tab) {
							double v = ((double) s.getNbInter()) / ((double) n);
							res.tMax = Math.max(res.tMax, v);
							res.tMin = Math.min(res.tMin, v);
							res.tAverage += v;
						}
						Log.getInstance().info("mod="+mod+" n=" + n + "m=" + m + " nbT=" + nbT + "/" + nbTest);
					}
					res.tAverage /= nbTest;
					for (int i = 0; i <= i_n; i++) {
						String str = "" + tab_n[i];
						for (int j = 0; (j < 3 && i < i_n) || (j <= i0 && i == i_n); j++) {
							Result r = tabResult[i][j];
							str += "\t" + r.tMax + "\t" + r.tAverage + "\t" + r.tMin;
						}
						Log.getInstance().info(str);
					}
				}
			}

		}

	}

}
