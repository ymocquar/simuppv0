package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import impl.AverageDouble;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;
import network.UniformSched;

public class MainAverageDelta {

	public static long getNbTour(int n, Scheduler random, int mod) {
		List<IValue> values = new ArrayList<>();

		NetworkFactory fact = NetworkFactory.getInstance(n);

		double res = 0.;
		for (int i = 0; i < n; i++) {
			double val = (i % mod == 0) ? -1. : 1.;
			res += val;
			values.add(new AverageDouble(n, val));
		}
		fact.setScheduler(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		long nbGood = 0;
		final int resOK = (int) res;
		int nbTour = 0;
		while (nbGood != n) {
			int nb = (nbGood > n / 2) ? n / 10 : n;
			netWork.process1(nb);
			nbGood = values.parallelStream().filter(v -> ((AverageDouble) v).getResult() == resOK).count();
			nbTour += nb;
		}
		// if (nbTour > maxConv) {
		// secondConv = maxConv;
		// maxConv = nbTour;
		// } else {
		// if (nbTour > secondConv) {
		// secondConv = nbTour;
		// }
		// }
		return nbTour;
	}

	public static void main(String[] args) {
		int n = 1000;

		for (int nbTest = 10000; nbTest <= 10000000; nbTest *= 10) {

			for (int mod = 2; mod <= 4; mod += 2) {

				List<Scheduler> tab = new ArrayList<>();

				for (int i = 0; i < nbTest; i++) {
					tab.add(new UniformSched());
				}

				final int nPar = n;
				final int modPar=mod;
				long[] res = tab.parallelStream().mapToLong(r -> getNbTour(nPar, r, modPar) ).toArray();

				Arrays.sort(res);

				// String str="";
				// for ( int i = 490 ; i < 10000 ; i += 500)
				// str += ","+((double)res[i])/n;
				// str += ","+((double)res[9999])/n;

				Log.getInstance().info("n=" + n + " nbTest=" + nbTest + " mod="+mod);

				double[] tabMult = { 2.5, 2., 2. };
				int i = 0;
				for (double delta = 0.5; delta > 10. / nbTest; delta /= tabMult[i % 3], i++) {
					int indice = nbTest - (int) Math.round(delta * nbTest);
					Log.getInstance().info("" + delta + "\t" + ((double) res[indice]) / n);
				}
			}
		}

	}

}
