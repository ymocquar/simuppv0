package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import impl.AverageIntWithCount;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;
import network.UniformSched;

public class MainAverageIntEps {

	static class Result {
		int n;
		List<Double> res = new ArrayList<>();
	}

	public static void testAverage(AverageIntWithCount.DataCountAverageInt random) {
		List<IValue> values = new ArrayList<>();
		int n = random.n;

		NetworkFactory fact = NetworkFactory.getInstance(n);
		int borne=2;
		double note = 1.;
		for (int b = 2; b < 2 * n / 3; b += 2) {
			double ell = ((double) (b * random.m)) / n;
			ell -= Math.floor(ell);
			double newnote = Math.abs(ell - 0.5);
			if (newnote < note) {
				note = newnote;
				borne = b;
			}
		}
		for (int i = 0; i < borne; i++)
		{
			values.add(new AverageIntWithCount(random, 1));
		}
		for (int i = borne; i < n; i++) {
			// int sens = random.nextBoolean() ? 1 : -1;
			int sens = (i % 2 == 0) ? 1 : -1;
			values.add(new AverageIntWithCount(random, sens));
		}
		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		int borneSec = 100 * n * (int) Math.log(n);
		long k = 0;
		while (random.getDiff() > 2) {
			netWork.process1();
			k++;
			if (k > borneSec) {
				Log.getInstance().error("ERREUR k=" + k + " r=" + random);
				break;
			}
		}
		int minVal = values.parallelStream().mapToInt(v -> ((AverageIntWithCount) v).val).min().getAsInt();
		int maxVal = values.parallelStream().mapToInt(v -> ((AverageIntWithCount) v).val).max().getAsInt();
		if (maxVal - minVal > 2) {
			Log.getInstance().error("ERROR min=" + minVal + " max=" + maxVal + " r=" + random);
		}
		random.result = ((double) k) / ((double) n);
	}

	public static void main(String[] args) {
		int nbTest = 1000;
		int paquetTest = 100;
		// int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000,
		// 20000, 50000, 100000, 200000, 500000,
		// 1000000};
		// int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000,
		// 20000, 50000, 100000, 200000, 500000,
		// 1000000 };
		int[] tab_n = { 1000, 10000, 100000 };
		// double[] tabDelta = { 0.5, 0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002,
		// 0.001, 0.0005, 0.0002, 0.0001, 0.00005,
		// 0.00002, 0.00001 };
		double[] tabEpsilon = { 0.5, 0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001, 0.0005, 0.0002, 0.0001, 0.00005,
				0.00002, 0.00001 };

		Result[][] tabResult = new Result[tabEpsilon.length][tab_n.length];
		// Result[] tabResult = new Result[tab_n.length];

		for (int i_e = 0; i_e < tabEpsilon.length; i_e++) {
			for (int i_n = 0; i_n < tab_n.length; i_n++) {

				int n = tab_n[i_n];

				Result res = new Result();
				tabResult[i_e][i_n] = res;
				// tabResult[i_n] = res;
				res.n = n;

				for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {
					List<AverageIntWithCount.DataCountAverageInt> tab = new ArrayList<>();
					for (int i = 0; i < paquetTest; i++) {
						 tab.add(new
						 AverageIntWithCount.DataCountAverageInt(n,
						 (int) Math.ceil(3. / (4. * tabEpsilon[i_e]))));
//						tab.add(new AverageIntWithCount.DataCountAverageInt(n, 75));
					}

					tab.parallelStream().forEach(r -> testAverage(r));

					for (AverageIntWithCount.DataCountAverageInt s : tab) {
						res.res.add(s.result);
					}
					Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
				}
				Collections.sort(res.res);
				// for (int i = 0; i <= i_n; i++) {
				// Result r = tabResult[i];
				// String str = "" + tab_n[i];
				// // str += "\t";
				// //
				// // str += r.res.get(nbTest/2 - 1);
				//
				// for (int j = 10; j <= nbTest / 10; j *= 10) {
				// // if ( j!= 10 )
				// str += "\t";
				//
				// str += r.res.get(nbTest - nbTest / j);
				// }
				// Log.getInstance().info(str);
				// }
				// for (int i = 0; i < tabDelta.length; i++) {
				// String str = "" + tabDelta[i];
				// for (int j = 0; j <= i_n; j++) {
				// Result r = tabResult[j];
				// // if ( j!= 10 )
				// str += "\t";
				//
				// str += r.res.get((int) (((double) nbTest) * (1. -
				// tabDelta[i]) - 0.98));
				// }
				// Log.getInstance().info(str);
				// }
				for (int i = 0; i <= i_e; i++) {
					String str = "" + tabEpsilon[i];
					int borne = (i == i_e) ? i_n + 1 : tab_n.length;
					for (int j = 0; j < borne; j++) {
						Result r = tabResult[i][j];
						// if ( j!= 10 )
						str += "\t";

						str += r.res.get(nbTest / 2);
					}
					Log.getInstance().info(str);
				}

			}
		}
	}
}
