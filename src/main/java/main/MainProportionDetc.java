package main;

import java.util.ArrayList;
import java.util.List;

import impl.ProportionDetcWithCount;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;

public class MainProportionDetc {

	static class Result {
		int n;
		double sum = 0;
		int nbT = 0;
		double moy;
		double min=Double.MAX_VALUE;
		double max=0.;
	}

	public static void testProportion(ProportionDetcWithCount.DataCountProportion random) {
		List<IValue> values = new ArrayList<>();
		int n = random.n;
		
//		Log.getInstance().trace("Debut testProportion");

		NetworkFactory fact = NetworkFactory.getInstance(n);
		int borne=2;
		double note = 1.;
		for (int b = 2; b < 2 * n / 3; b += 2) {
			double ell = ((double) (b * random.m)) / n;
			ell -= Math.floor(ell);
			double newnote = Math.abs(ell - 0.5);
			if (newnote < note) {
				note = newnote;
				borne = b;
				if ( note < 0.001 )
					break;
			}
			
		}
		for (int i = 0; i < borne; i++) {
			values.add(new ProportionDetcWithCount(random, 1));
		}

		for (int i = borne; i < n; i++) {
			int sens = (i%2==0) ? 1 : -1;
			values.add(new ProportionDetcWithCount(random, sens));
		}
		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		long k = 0;
		while (random.getNbS() < n) {
			netWork.process1();
			k++;
		}
		random.result = ((double) k) / ((double) n);
	}

	public static void main(String[] args) {
		int nbTest = 1000;
		int paquetTest = 100;
		int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000,
				1000000};
//		int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000,
//				1000000, 2000000, 5000000, 10000000 };
		// int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000,
		// 20000, 50000, 100000, 200000, 500000,
		// 1000000 };
		// int[] tab_n = { 1000, 10000, 100000 };
		// double[] tabDelta = { 0.5, 0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002,
		// 0.001, 0.0005, 0.0002, 0.0001, 0.00005,
		// 0.00002, 0.00001 };
		double[] tabEpsilon = { 0.1, 0.001, 0.00001 };

		Result[][] tabResult = new Result[tabEpsilon.length][tab_n.length];
		// Result[] tabResult = new Result[tab_n.length];

		for (int i_n = 0; i_n < tab_n.length; i_n++) {
			for (int i_e = 0; i_e < tabEpsilon.length; i_e++) {

				int n = tab_n[i_n];

				Result res = new Result();
				tabResult[i_e][i_n] = res;
				/// tabResult[i_n] = res;
				res.n = n;

				for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {
					List<ProportionDetcWithCount.DataCountProportion> tab = new ArrayList<>();
					for (int i = 0; i < paquetTest; i++) {
						tab.add(new ProportionDetcWithCount.DataCountProportion(n, 0.000001 , tabEpsilon[i_e]));
					}

					tab.parallelStream().forEach(r -> testProportion(r));

					for (ProportionDetcWithCount.DataCountProportion s : tab) {
						res.sum += s.result;
						res.nbT++;
						res.max = Math.max(res.max, s.result);
						res.min = Math.min(res.min, s.result);
					}
					Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
				}

				res.moy = res.sum / res.nbT;
				// for (int i = 0; i <= i_n; i++) {
				// String str = "" + tab_n[i];
				// str += "\t";
				// Result r = tabResult[i];
				// for (int j = 10; j < nbTest / 10; j *= 10) {
				// // if ( j!= 10 )
				// str += "\t";
				//
				// str += r.res.get(nbTest - nbTest / j);
				// }
				// Log.getInstance().info(str);
				// }
				// for (int i = 0; i < tabDelta.length; i++) {
				// String str = "" + tabDelta[i];
				// for (int j = 0; j <= i_n; j++) {
				// Result r = tabResult[j];
				// // if ( j!= 10 )
				// str += "\t";
				//
				// str += r.res.get((int) (((double) nbTest) * (1. -
				// tabDelta[i]) - 0.98));
				// }
				// Log.getInstance().info(str);
				// }
				for (int j = 0; j <= i_n; j++) {
					String str = "" + tab_n[j];
					int borne = (j == i_n) ? i_e + 1 : tabEpsilon.length;
					for (int i = 0; i < borne; i++) {
						Result r = tabResult[i][j];
						// if ( j!= 10 )
						str += "\t" + r.moy + "\t" + r.min + "\t" + r.max;
					}
					Log.getInstance().info(str);
				}

			}
			// }
		}
	}
}
