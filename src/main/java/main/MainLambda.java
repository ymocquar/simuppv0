package main;

import java.util.ArrayList;
import java.util.List;

import impl.CountPrec;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;

public class MainLambda {

	static class Result {
		int n;
		int m;
		int nA;
		double tMin;
		double tMax;
		double tAverage;
	}

	public static double testAverage(int nA, int n, CountPrec.DataCount random) {
		List<IValue> values = new ArrayList<>();

		NetworkFactory fact = NetworkFactory.getInstance(n);

		for (int i = 0; i < n; i++) {
			values.add(new CountPrec(random, random.m, (i < nA) ? 1 : -1));
		}
		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		int k = 0;
		while (random.getGap() > 2) {
			netWork.process1();
			k++;
		}
		return ((double) k) / ((double) n);
	}

	public static void main(String[] args) {
		int nbTest = 10000;
		int paquetTest = 100;

		int n = 10000;
		int m = 100;
		Result [] tabResult = new Result[200];
		for (int nA=5000 ; nA < 5200 ; nA++ ) {

			Result res = tabResult[ nA-5000] = new Result();
			res.m = m;
			res.n = n;
			res.nA = nA;
			res.tMin = 10000.;
			for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

				List<CountPrec.DataCount> tab = new ArrayList<>();

				for (int i = 0; i < paquetTest; i++) {
					tab.add(new CountPrec.DataCount(m));
				}

				final int nPar = n;
				final int nA_Par = nA;

				tab.parallelStream().forEach(r -> testAverage(nA_Par, nPar, r));

				for (CountPrec.DataCount s : tab) {
					double v = ((double) s.getNbInter()) / ((double) n);
					res.tMax = Math.max(res.tMax, v);
					res.tMin = Math.min(res.tMin, v);
					res.tAverage += v;
				}
				Log.getInstance().info("nA=" + nA + " n=" + n + "m=" + m + " nbT=" + nbT + "/" + nbTest);
			}
			res.tAverage /= nbTest;
			int b=nA-5000;
			for (int i = 0; i <= b; i++) {
				Result r = tabResult[i];
				String str = "" + ((double) (r.m*(2*r.nA-r.n)))/((double)n)+"\t" + r.tMax + "\t" + r.tAverage + "\t" + r.tMin;
				Log.getInstance().info(str);
			}

		}

	}

}
