package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import impl.CountMinSimple;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;
import network.UniformSched;

public class MainMoyenneTwoChoice {

	static class SchedulerResult extends UniformSched implements Scheduler {
		long[] result = new long[30];

	}

	public static void getDistri50(int n, SchedulerResult random) {
		List<IValue> values = new ArrayList<>();

		NetworkFactory fact = NetworkFactory.getInstance(n);

		double res = 0.;
		for (int i = 0; i < n; i++) {
			values.add(new CountMinSimple());
		}
		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		netWork.process1(n * 50);
		Arrays.fill(random.result, 0);

		for (IValue v : values) {
			int ind = ((CountMinSimple) v).getResult() - 30; // 30 = 50-20
			if (ind < 30 && ind >= 0)
				random.result[ind]++;
		}
	}

	public static void main(String[] args) {
		int nbTest = 5000;
		int paquetTest = 50;
		int nbR = 1;
		double[][] res = new double[5][30];
		int[] tabN = new int[5];
		int i_n = 0;
		for (int n = 1000; n <= 10000000; n *= 10, i_n++) {

			tabN[i_n] = n;

			for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {
				List<SchedulerResult> tab = new ArrayList<>();

				for (int i = 0; i < paquetTest; i++) {
					tab.add(new SchedulerResult());
				}

				final int nPar = n;

				tab.parallelStream().forEach(r -> getDistri50(nPar, r));

				for (SchedulerResult s : tab) {
					for (int i = 0; i < 30; i++) {
						res[i_n][i] += s.result[i];
					}
				}
				Log.getInstance().info("n="+n+" nbT="+nbT+"/"+nbTest);
			}
			for (int i = 0; i < 30; i++) {
				double dn=n;
				dn *= nbTest;
				res[i_n][i] /= dn;
			}

			for (int i = 0; i < 30; i++) {
				String str = "nbTest=" + nbTest + " n=" + n + " " + (i - 20);
				for (int j = 0; j <= i_n; j++) {
					str += "\t" + res[j][i];
				}
				Log.getInstance().info(str);
			}
			for (int i = 0; i < 30; i++) {
				String str = "nbTest=" + nbTest + " n=" + n + " " + (i - 20);
				for (int j = 0; j <= i_n; j++) {
					str += " & " + res[j][i];
				}
				Log.getInstance().info(str+"\\\\");
			}

		}

	}

}
