package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import impl.AverageDouble;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;
import network.Scheduler;
import network.UniformSched;

public class MainAverage {

	public static long getNbTour(int n, Scheduler random, int mod) {
		List<IValue> values = new ArrayList<>();

		NetworkFactory fact = NetworkFactory.getInstance(n);

		double res = 0.;
		for (int i = 0; i < n; i++) {
			double val = (i % mod == 0) ? -1. : 1.;
			res += val;
			values.add(new AverageDouble(n, val));
		}
		fact.setScheduler(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();
		long nbGood = 0;
		final int resOK = (int) res;
		int nbTour = 0;
		while (nbGood != n) {
			int nb = (nbGood > n / 2) ? n / 10 : n;
			netWork.process1(nb);
			nbGood = values.parallelStream().filter(v -> ((AverageDouble) v).getResult() == resOK).count();
			nbTour += nb;
		}
		// if (nbTour > maxConv) {
		// secondConv = maxConv;
		// maxConv = nbTour;
		// } else {
		// if (nbTour > secondConv) {
		// secondConv = nbTour;
		// }
		// }
		return nbTour;
	}

	public static void main(String[] args) {
		int n = 10;
		int nbTest = 100000;
		double resMod2=0.;
		double resMod4=0.;
		for (; n <= 1000000; n *= 2) {
			if ( n >20000) nbTest = 10000;
			for (int mod = 2; mod <= 4; mod += 2) {

				List<Scheduler> tab = new ArrayList<>();

				for (int i = 0; i < nbTest; i++) {
					tab.add(new UniformSched());
				}

				final int nPar = n;
				final int modPar = mod;

				long[] res = tab.parallelStream().mapToLong(r -> getNbTour(nPar, r, modPar)).toArray();

				Arrays.sort(res);

				// String str="";
				// for ( int i = 490 ; i < 10000 ; i += 500)
				// str += ","+((double)res[i])/n;
				// str += ","+((double)res[9999])/n;
				if ( mod == 2 )
					resMod2=((double) res[nbTest - nbTest/1000]) / n ;
				else
					resMod4=((double) res[nbTest - nbTest/1000]) / n ;
					
			}
			Log.getInstance().info("n=" + n + " nbTest=" + nbTest + " str="+n+"\t"+resMod2+"\t"+resMod4);
		}

	}

}
