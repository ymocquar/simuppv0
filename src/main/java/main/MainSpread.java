//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
//
// Ce logiciel a été écrit par Yves Mocquard en 2019
// 
// MainCounting
//
// Ce logiciel générer des données utilisables par des graphiques
// Il fonctionne en multithread
// le multithread est basé sur la technologie Stream.
//

package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import impl.SpreadWithCount;
import log.Log;

public class MainSpread {


	// classe interne permettant de stocker les résultats
	static class Result {
		int n;
		List<Double> res = new ArrayList<>();
	}

	static long seed = 0;
	static boolean isForced = false;

	static String strForced() {
		return isForced ? " seed forcé" : "";
	}

	static String getSignature() {
		return "\t# "+ MainSpread.class.getName()+" v0.0 seed=" + seed + strForced();
	}

	static String getSignature(int nbT) {
		return getSignature() + " N=" + nbT + strForced();
	}

	// Méthode main
	// Il y a un parametre optionnel c'est le seed qui permet de reproduire
	// exactement la même simulation
	//
	// sans paramètre un seed est généré automatiquement.
	public static void main(String[] args) {
		int nbTest = 10000;
		int paquetTest = 1000;
		Random rand = new Random();
		int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000,
				1000000, 2000000, 5000000, 10000000 };

		Result[] tabResult = new Result[tab_n.length];

		// Result[] tabResult = new Result[tab_n.length];

		seed = (args.length == 1) ? Long.parseLong(args[0]) : rand.nextLong();

		seed &= (1L << 32) - 1L;

		rand.setSeed(seed);

		isForced = (args.length == 1);

		for (int i_n = 0; i_n < tab_n.length; i_n++) {
			int n = tab_n[i_n];

			Result res = new Result();
			tabResult[i_n] = res;
			res.n = n;

			for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {

				List<SpreadWithCount.DataCountSpread> tab = new ArrayList<>();
				for (int i = 0; i < paquetTest; i++) {
					tab.add(new SpreadWithCount.DataCountSpread(n, rand.nextLong()));
				}
				tab.parallelStream().forEach(r -> SpreadWithCount.executePP(r, 2));

				for (SpreadWithCount.DataCountSpread s : tab) {
					res.res.add(s.result);
				}
				Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
			}
			Collections.sort(res.res);

			String strDelta = "";
			for (double d = 0.1; d > 9.99 / res.res.size(); d /= 10.) {
				strDelta += "\t" + (float) d;
			}

			Log.getInstance().info("\t# n" + strDelta + getSignature());
			for (int i = 0; i <= i_n; i++) {
				String str = "\t" + tab_n[i];
				Result r = tabResult[i];
				for (double d = 0.1; d > 9.99 / r.res.size(); d /= 10.) {
					str += "\t" + r.res.get(r.res.size() - (int) Math.round(d * r.res.size()));
				}
				Log.getInstance().info(str + getSignature(r.res.size()));

			}
		}

	}
}
