package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import impl.CountMinWithGap;
import log.Log;
import network.IValue;
import network.Network;
import network.NetworkFactory;

public class MainCountMinGapDelta {

	static class Result {
		int n;
		List<Integer> res = new ArrayList<>();
	}

	public static void testGap(int nb, CountMinWithGap.GapCountMin random) {
		List<IValue> values = new ArrayList<>();
		int n = random.n;

		NetworkFactory fact = NetworkFactory.getInstance(n);

		for (int i = 0; i < n; i++) {
			values.add(new CountMinWithGap(random));
		}
		fact.setRandom(random);
		fact.setValues(values);
		fact.setFullConnected(true);

		Network netWork = fact.getInstanceOfNetWork();

		long k = 0;
		netWork.process1(50 * n);
//		int nbProc = n / 100;
		int nbProc = 100;
		while (k < nb) {
			netWork.process1(nbProc);
			k++;
			random.result.add(random.getGap());
		}
	}

	public static void main(String[] args) {
		int nbTest = 1000;
		int paquetTest = 10;
		// int[] tab_n = { 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000,
		// 20000, 50000, 100000, 200000, 500000,
		// 1000000, 2000000, 5000000, 10000000 };
		int[] tab_n = { 1000, 10000, 100000 };
		// int[] tab_n = { 1000, 10000, 100000 };
		double[] tabDelta = { 0.5, 0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001, 0.0005, 0.0002, 0.0001, 0.00005,
				0.00002, 0.00001 };

		// Result[][] tabResult = new Result[tabEpsilon.length][tab_n.length];
		Result[] tabResult = new Result[tab_n.length];

		// for (int i_e = 0; i_e < tabEpsilon.length; i_e++) {
		for (int i_n = 0; i_n < tab_n.length; i_n++) {

			int n = tab_n[i_n];

			Result res = new Result();
			tabResult[i_n] = res;
			res.n = n;

			for (int nbT = 0; nbT < nbTest; nbT += paquetTest) {
				List<CountMinWithGap.GapCountMin> tab = new ArrayList<>();
				for (int i = 0; i < paquetTest; i++) {
					tab.add(new CountMinWithGap.GapCountMin(n));
				}

				tab.parallelStream().forEach(r -> testGap(10000, r));

				for (CountMinWithGap.GapCountMin s : tab) {
					res.res.addAll(s.result);
				}
				Log.getInstance().info(" n=" + n + " nbT=" + nbT + "/" + nbTest);
			}
			Collections.sort(res.res);
			int tot = nbTest * 10000;
			for (int i = 0; i < tabDelta.length; i++) {
				String str = "" + tabDelta[i];
				for (int j = 0; j <= i_n; j++) {
					Result r = tabResult[j];
					// if ( j!= 10 )
					str += "\t";

					str += r.res.get((int) (((double) tot) * (1. - tabDelta[i]) - 0.98));
				}
				Log.getInstance().info(str);
			}

		}
	}
}
