//
// Copyright 2015-2019 Yves Mocquard
//
//
// This file is a part of SimuPPv1b
// SimuPPv1b is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//
package network;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import impl.HowMany;

public class TestNetworkFactory {
	
	NetworkFactory fact;
	
	@Before
	public void setUp()
	{
		fact = NetworkFactory.getInstance(100);
		List<IValue> list = getListValues(fact.getNbNodes());
		fact.setValues(list);
	}

	private List<IValue> getListValues(int nb) {
		List<IValue> list=new ArrayList<>();
		for( int i= 0 ; i < nb; i++ )
		{
			list.add(new HowMany());
		}
		return list;
	}

	@Test
	public void TestAfterGetInstance()
	{
		assertEquals(100,fact.getNbNodes());
		assertEquals(1, fact.getNbNeighborMin());
		assertEquals(7,fact.getNbNeighborMax());
		assertFalse( fact.isFullConnected());

	}
	
	@Test 
	public void TestSetNeightbourMin()
	{
		fact.setNbNeighborMin(4);
		assertEquals(4,fact.getNbNeighborMin());
		assertEquals(7,fact.getNbNeighborMax());
		
		fact.setNbNeighborMin(10);
		assertEquals(10,fact.getNbNeighborMin());
		assertEquals(10,fact.getNbNeighborMax());
			
	}
	
	@Test 
	public void TestSetNeightbourMax()
	{
		fact.setNbNeighborMin(4);
		fact.setNbNeighborMax(20);
		assertEquals(4,fact.getNbNeighborMin());
		assertEquals(20,fact.getNbNeighborMax());
		
		fact.setNbNeighborMax(3);
		assertEquals(3,fact.getNbNeighborMin());
		assertEquals(3,fact.getNbNeighborMax());
			
	}
	
	@Test 
	public void TestFullConnected()
	{
		fact.setFullConnected(true);
		assertTrue(fact.isFullConnected());
		fact.setFullConnected(true);
		assertTrue(fact.isFullConnected());
		fact.setFullConnected(false);
		assertFalse(fact.isFullConnected());
		fact.setFullConnected(false);
		assertFalse(fact.isFullConnected());

	}
	@Test 
	public void TestRingGraph()
	{
		fact.setRingGraph(true);
		assertTrue(fact.isRingGraph());
		fact.setRingGraph(true);
		assertTrue(fact.isRingGraph());
		fact.setRingGraph(false);
		assertFalse(fact.isRingGraph());
		fact.setRingGraph(false);
		assertFalse(fact.isRingGraph());
	}
	
	@Test
	public void TestSetNbNodes()
	{
		fact.setNbNodes(120);
		assertEquals(120,fact.getNbNodes());
	}
	
	@Test
	public void TestSetValuesNormalCase()
	{
		List<IValue> list = getListValues(fact.getNbNodes());
		fact.setValues(list);
		assertEquals(list,fact.getValues());
	}

	@Test(expected=IllegalArgumentException.class)
	public void TestSetValuesNotNormalCase()
	{
		List<IValue> list = getListValues(55);
		fact.setValues(list);	
	}
	@Test
	public void TestGetInstanceOfNetwork()
	{
		Network netWork = fact.getInstanceOfNetWork();
		assertNotNull(netWork);		
	}
	@Test(expected=IllegalArgumentException.class)
	public void TestGetInstanceOfNetworkBad1()
	{
		fact.setNbNeighborMin(-1);
		Network netWork = fact.getInstanceOfNetWork();
		assertNotNull(netWork);		
	}
	@Test(expected=IllegalArgumentException.class)
	public void TestGetInstanceOfNetworkBad2()
	{
		fact.setNbNeighborMax(120);
		Network netWork = fact.getInstanceOfNetWork();
		assertNotNull(netWork);		
	}
	@Test
	public void TestGetInstanceOfNetwork2()
	{
		fact.setFullConnected(true);
		fact.setNbNeighborMin(-1);
		Network netWork = fact.getInstanceOfNetWork();
		assertNotNull(netWork);		
	}
	@Test
	public void TestGetInstanceOfNetwork3()
	{
		fact.setFullConnected(true);
		fact.setNbNeighborMin(120);
		Network netWork = fact.getInstanceOfNetWork();
		assertNotNull(netWork);		
	}
	@Test(expected=NullPointerException.class)
	public void TestGetInstanceOfNetworkBad3()
	{
		NetworkFactory fact2 = NetworkFactory.getInstance(100);
		Network netWork = fact2.getInstanceOfNetWork();
		assertNotNull(netWork);		

	}
	@Test(expected=IllegalArgumentException.class)
	public void TestGetInstanceOfNetworkBad4()
	{
		fact.setNbNodes(45);
		Network netWork = fact.getInstanceOfNetWork();
		assertNotNull(netWork);		

	}
	
	@Test
	public void testGetRandom()
	{
		Scheduler random = fact.getRandom();
		assertNotNull(random);
	}
}
